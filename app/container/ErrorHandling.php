<?php

declare(strict_types=1);

use App\Factory\LoggerFactory;
use App\Middleware\ExceptionHandlerMiddleware;
use App\Renderer\JsonRenderer;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Slim\Views\Twig;

return [
    ExceptionHandlerMiddleware::class => function (ContainerInterface $container) {
        return new ExceptionHandlerMiddleware(
            $container->get(ResponseFactoryInterface::class),
            $container->get(JsonRenderer::class),
            $container->get(Twig::class),
            $container->get(LoggerFactory::class)
        );
    }
];
