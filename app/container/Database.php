<?php

declare(strict_types=1);

use App\Repository\Repository;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Tools\DsnParser;
use Psr\Container\ContainerInterface;

return [
    Connection::class => function () {
        $parser = new DsnParser();
        $params = $parser->parse($_ENV['DATABASE_DSN']);

        $configuration = new Configuration();

        $connection = DriverManager::getConnection($params, $configuration);

        return $connection;
    },

    Repository::class => function (ContainerInterface $container) {
        return new Repository($container->get(Connection::class));
    },
];
