<?php

declare(strict_types=1);

use App\Domain\User\Entity\User;
use App\Domain\User\Service\RefreshUserFromSessionService;
use App\Factory\LoggerFactory;
use App\Middleware\ExceptionHandlerMiddleware;
use App\Renderer\JsonRenderer;
use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use Slim\App;
use Slim\Csrf\Guard;
use Slim\Factory\AppFactory;
use Slim\Interfaces\RouteParserInterface;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Symfony\Component\Filesystem\Path;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;

return [
    'settings' => fn () => require Path::normalize(__DIR__ . '/settings.php'),

    'request_id' => fn () => substr(strtoupper(bin2hex(random_bytes(32))), 0, 6),

    App::class => function (ContainerInterface $container) {
        $app = AppFactory::createFromContainer($container);

        (include Path::normalize(ROOT_DIR . '/app/routes.php'))($app);

        (include Path::normalize(ROOT_DIR . '/app/middleware.php'))($app);

        return $app;
    },

    ResponseFactoryInterface::class => function (ContainerInterface $container) {
        return $container->get(Psr17Factory::class);
    },

    ServerRequestFactoryInterface::class => function (ContainerInterface $container) {
        return $container->get(Psr17Factory::class);
    },

    StreamFactoryInterface::class => function (ContainerInterface $container) {
        return $container->get(Psr17Factory::class);
    },

    UriFactoryInterface::class => function (ContainerInterface $container) {
        return $container->get(Psr17Factory::class);
    },

    RouteParserInterface::class => function (ContainerInterface $container) {
        return $container->get(App::class)->getRouteCollector()->getRouteParser();
    },

    TwigMiddleware::class => function (ContainerInterface $container) {
        return TwigMiddleware::createFromContainer(
            $container->get(App::class),
            Twig::class
        );
    },

    Session::class => function () {
        if (PHP_SAPI === "cli") {
            return new Session(new MockArraySessionStorage());
        } else {
            return new Session(
                new NativeSessionStorage(
                    [
                    'name' => strtolower($_ENV['APP_NAME']),
                    'cache_expire' => 0,
                    'cookie_lifetime' => 2592000,
                    'gc_maxlifetime' => 604800
                    ]
                )
            );
        }
    },

    LoggerFactory::class => function () {
        return new LoggerFactory();
    },

    Guard::class => function (ContainerInterface $container) {
        return new Guard($container->get(ResponseFactoryInterface::class));
    },

    User::class => function (ContainerInterface $container) {
        return (new RefreshUserFromSessionService(
            $container,
        ))->refreshUser();
    },
];
