<?php

declare(strict_types=1);

use App\Domain\User\Entity\User;
use App\Extension\Twig\EnumExtension;
use App\Extension\Twig\SlimCsrfExtension;
use App\Extension\Twig\WebpackAssetLoader;
use Psr\Container\ContainerInterface;
use Slim\Csrf\Guard;
use Slim\Views\Twig;
use Symfony\Component\HttpFoundation\Session\Session;
use Twig\Extra\Html\HtmlExtension;

return [
    Twig::class => function (ContainerInterface $container) {

        $session = $container->get(Session::class);
        $twig = Twig::create(
            [
                ROOT_DIR . "/templates",
            ],
            [
                'cache' => ROOT_DIR . "/tmp/twig",
                'cache_enabled' => !DEBUG,
                'debug' => DEBUG
            ]
        );
        $twig->getEnvironment()->addGlobal(
            "flash",
            $session->getFlashBag()->all()
        );
        $twig->getEnvironment()->addGlobal(
            'request_id',
            $container->get('request_id')
        );
        $appGlobal = [
            'debug' => DEBUG,
            'name' => $_ENV['APP_NAME'],
            'fullname' => $_ENV['APP_FULLNAME'],
            'uri' => $_ENV['APP_URI'],
            'environment' => $_ENV['APP_ENV'],
            'version' => VERSION,
            'user' => $container->get(User::class),
            'display_tz' => $_ENV['APP_TZ_H']
        ];
        $twig->addExtension(new \Twig\Extension\DebugExtension());
        $twig->addExtension(new SlimCsrfExtension($container->get(Guard::class)));
        $twig->addExtension(new WebpackAssetLoader(ROOT_DIR . "/public", DEBUG));
        $twig->addExtension(new EnumExtension());
        $twig->addExtension(new HtmlExtension());
        $twig->getEnvironment()->addGlobal('app', $appGlobal);
        $twig->getEnvironment()->getExtension(\Twig\Extension\CoreExtension::class)->setDateFormat(
            DateTime::ATOM,
            '%a minutes'
        );
        return $twig;
    },
];
