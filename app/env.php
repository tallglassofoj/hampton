<?php

declare(strict_types=1);


$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . "/../");
$dotenv->load();
$dotenv->required(['APP_NAME', 'APP_FULLNAME', 'APP_URI', 'APP_SECRET'])->notEmpty();
$dotenv->required('APP_ENV')->allowedValues(['prod', 'test', 'dev', 'local']);
$dotenv->required('APP_TZ_H')->allowedValues(['Pacific', 'Mountain', 'Central', 'Eastern']);
$dotenv->required('DATABASE_DSN')->notEmpty();

if (!defined('DEBUG')) {
    match ($_ENV['APP_ENV']) {
        default => define('DEBUG', false),
        'test', 'dev', 'local' => define('DEBUG', true)
    };
}

require_once __dir__  . "/version.php";

define("VERSION", VERSION_MAJOR . '.' . VERSION_MINOR . '.' . VERSION_PATCH . VERSION_TAG);
