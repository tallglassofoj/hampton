<?php

declare(strict_types=1);

$settings = include __DIR__ . '/defaults.php';

$configFiles = [
    __DIR__ . sprintf('/../config/%s.php', $_SERVER['APP_ENV'] ?? 'prod'),
    __DIR__ . sprintf('/../env.php')
];

foreach ($configFiles as $configFile) {
    if (!file_exists($configFile)) {
        continue;
    }

    $local = include $configFile;
    if (is_callable($local)) {
        $settings = $local($settings);
    }
}

//TODO: Move this to the container?


$settings['secret'] = $_ENV['APP_SECRET'] ?? throw new Exception("Application secret token is missing from .env");
$settings['url'] = $_ENV['APP_URI'] ?? throw new Exception("APP_URI is missing from .env");



return $settings;
