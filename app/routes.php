<?php

// Define app routes

use App\Domain\User\Data\Permissions;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\App;
use Slim\Routing\RouteCollectorProxy;

return function (App $app) {
    $app->group('', function (RouteCollectorProxy $app) {
        $app->get(
            '/',
            \App\Action\Home\HomeAction::class
        )->setName('home');

        $app->map(
            ['GET', 'POST'],
            '/register',
            \App\Action\User\Auth\UserRegistrationAction::class
        )->setName('register');

        $app->map(
            ['GET', 'POST'],
            '/login',
            \App\Action\User\Auth\UserLoginAction::class
        )->setName('login');

        $app->post(
            '/logout',
            \App\Action\User\Auth\UserLogoutAction::class
        )->setName('logout');
    })->add(function (ServerRequestInterface $request, RequestHandlerInterface $handler) {
        $request = $request->withAttribute(
            'user',
            false
        );
        $response = $handler->handle($request);
        return $response;
    });

    $app->group('', function (RouteCollectorProxy $app) {
        $app->get(
            '/calendar',
            \App\Action\Calendar\CalendarAction::class
        )->setName('calendar');
        $app->get(
            '/calendar/events',
            \App\Action\Calendar\GetEventsAction::class
        )->setName('events');
        $app->map(
            ['GET', 'POST'],
            '/resetPassword/required',
            \App\Action\User\Auth\ForcedPasswordResetAction::class
        )->setName('password.reset.forced');
    });

    $app->group(
        '/event',
        function (RouteCollectorProxy $app) {
            //Event listing
            $app->get(
                '/list',
                \App\Action\Event\ListEventsAction::class
            )->setName('event.list');

            //Single event
            $app->group(
                '/{id:[0-9]+}',
                function (RouteCollectorProxy $app) {
                    $app->get(
                        '',
                        \App\Action\Event\ViewEventAction::class
                    )->setName('event.view');

                    $app->map(
                        ['GET', 'POST'],
                        '/edit',
                        \App\Action\Event\EditEventAction::class
                    )->setName('event.edit')->add(function (ServerRequestInterface $request, RequestHandlerInterface $handler) {
                        $request = $request->withAttribute(
                            'flag',
                            Permissions::SCHEDULE_EVENTS
                        );
                        $response = $handler->handle($request);
                        return $response;
                    });

                    //Sentence and shift requests
                    $app->group('', function (RouteCollectorProxy $app) {
                        $app->map(
                            ['GET', 'POST'],
                            '/sentence',
                            \App\Action\Visit\RequestSentenceAction::class
                        )->setName('sentence.request');
                        $app->map(
                            ['GET', 'POST'],
                            '/shift',
                            \App\Action\Visit\RequestShiftAction::class
                        )->setName('shift.request');
                    })->add(function (ServerRequestInterface $request, RequestHandlerInterface $handler) {
                        $request = $request->withAttribute(
                            'flag',
                            Permissions::REQUEST_BUILTIN
                        );
                        $response = $handler->handle($request);
                        return $response;
                    });

                    //Other types of visits
                    $app->map(
                        ['GET', 'POST'],
                        '/request/{profile:[0-9]+}',
                        \App\Action\Visit\RequestVisitAction::class
                    )->setName('visit.request')->add(function (ServerRequestInterface $request, RequestHandlerInterface $handler) {
                        $request = $request->withAttribute(
                            'flag',
                            Permissions::REQUEST_CUSTOM
                        );
                        $response = $handler->handle($request);
                        return $response;
                    });
                }
            );

            //Create and Edit Events
            $app->group('', function (RouteCollectorProxy $app) {
                $app->map(
                    ['GET', 'POST'],
                    '/new',
                    \App\Action\Event\NewEventAction::class
                )->setName('event.new');
                $app->map(
                    ['GET', 'POST'],
                    '/edit',
                    \App\Action\Event\EditEventAction::class
                )->setName('event.edit');
            })->add(function (ServerRequestInterface $request, RequestHandlerInterface $handler) {
                $request = $request->withAttribute(
                    'flag',
                    Permissions::SCHEDULE_EVENTS
                );
                $response = $handler->handle($request);
                return $response;
            });
        }
    );

    $app->group(
        '/inmate',
        function (RouteCollectorProxy $app) {
            $app->map(
                ['GET', 'POST'],
                '/new',
                \App\Action\Inmate\CreateInmateProfileAction::class
            )->setName('inmate.new');
        }
    );

    $app->group(
        '/guard',
        function (RouteCollectorProxy $app) {
            $app->map(
                ['GET', 'POST'],
                '/new',
                \App\Action\Guard\CreateGuardProfileAction::class
            )->setName('guard.new');
        }
    );

    $app->group(
        '/visit',
        function (RouteCollectorProxy $app) {
            $app->group(
                '/{visit:[0-9]+}',
                function (RouteCollectorProxy $app) {
                    $app->get(
                        '',
                        \App\Action\Visit\ViewVisitAction::class
                    )->setName('visit.view');
                    $app->map(
                        ['GET', 'POST'],
                        '/edit',
                        \App\Action\Visit\EditVisitAction::class
                    )->setName('visit.edit');
                    $app->post(
                        '/metadata/{key:[a-z]+}',
                        \App\Action\Visit\UpdateVisitMetadata::class
                    )->setName('visit.metadata');
                    $app->post(
                        '/approve',
                        \App\Action\Visit\ApproveVisitAction::class
                    )->setName('visit.approve');
                    $app->post(
                        '/disapprove',
                        \App\Action\Visit\DisapproveVisitAction::class
                    )->setName('visit.disapprove');
                }
            );
        }
    );

    //Roles
    $app->group(
        '/role',
        function (RouteCollectorProxy $app) {
            $app->group(
                '/{id:[0-9]+}',
                function (RouteCollectorProxy $app) {
                    $app->get('', \App\Action\Home\HomeAction::class);
                }
            );
            $app->get(
                '/list',
                \App\Action\Role\ListRolesAction::class
            )->setName('role.list');
            $app->map(
                ['GET', 'POST'],
                '/new',
                \App\Action\Role\CreateRoleAction::class
            )->setName('role.new');
        }
    );

    //Ranks
    $app->group(
        '/ranks',
        function (RouteCollectorProxy $app) {
            $app->get(
                '/list',
                \App\Action\Rank\ListRanksAction::class
            )->setName('ranks.list');
            $app->post(
                '/new',
                \App\Action\Rank\CreateRankAction::class
            )->setName('ranks.new');
            $app->map(
                ['GET', 'POST'],
                '/{id:[0-9]+}',
                \App\Action\Rank\EditRankAction::class
            )->setName('ranks.edit');
        }
    );

    $app->group(
        '/users',
        function (RouteCollectorProxy $app) {
            $app->get(
                '/list',
                \App\Action\User\ListUsersAction::class
            )->setName('users.list');
            $app->group(
                '/{user:[0-9]+}',
                function (RouteCollectorProxy $app) {
                    $app->map(
                        ['GET', 'POST'],
                        '[/{action:[a-z]+}]',
                        \App\Action\User\EditUserAction::class
                    )->setName('user.edit');
                }
            );
        }
    );
};
