<?php

declare(strict_types=1);

use DI\ContainerBuilder;
use Slim\App;
use Slim\Csrf\Guard;

define('ROOT_DIR', dirname(__DIR__));

require_once  __DIR__ . '/../vendor/autoload.php';
require_once  __DIR__ . '/env.php';

$containerBuilder = new ContainerBuilder();
$containerBuilder->useAutowiring(true);
$containerBuilder->useAttributes(true);

$containerBuilder->addDefinitions(
    __DIR__ . '/container/App.php',
    __DIR__ . '/container/Database.php',
    __DIR__ . '/container/Twig.php',
    __DIR__ . '/container/ErrorHandling.php'
);

$container = $containerBuilder->build();

match ($_ENV['APP_ENV']) {
    default => $container->get(App::class)->add(Guard::class),
    'local' => null
};

return $container->get(App::class);
