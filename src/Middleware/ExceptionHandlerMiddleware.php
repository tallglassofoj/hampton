<?php

namespace App\Middleware;

use App\Exception\ValidationException;
use App\Renderer\JsonRenderer;
use DomainException;
use Fig\Http\Message\StatusCodeInterface;
use InvalidArgumentException;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Exception\HttpException;
use Slim\Views\Twig;
use Throwable;

class ExceptionHandlerMiddleware implements MiddlewareInterface
{
    private Logger $logger;

    private ?array $errors = null;

    private bool $displayErrorDetails = false;

    public function __construct(
        private ResponseFactoryInterface $responseFactory,
        private JsonRenderer $jsonRenderer,
        private Twig $twig
    ) {
        $this->logger = new Logger('error.log');
        $stream = new RotatingFileHandler(ROOT_DIR . "/logs/error.log");
        $stream->setFormatter(new JsonFormatter());
        $this->logger->pushHandler($stream);
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            return $handler->handle($request);
        } catch (Throwable $exception) {
            if ($exception instanceof ValidationException) {
                $this->errors = $exception->getErrors();
            }
            return $this->render($exception, $request);
        }
    }

    /**
     * render
     *
     * Logs an exception and renders it as either json or html, depending on
     * whether or not the request specifies JSON
     *
     * @param Throwable $exception
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    private function render(
        Throwable $exception,
        ServerRequestInterface $request,
    ): ResponseInterface {
        $httpStatusCode = $this->getHttpStatusCode($exception);
        $response = $this->responseFactory->createResponse($httpStatusCode);
        // Log error
        $this->logger?->error(
            sprintf(
                '%s;Code %s;File: %s;Line: %s',
                $exception->getMessage(),
                $exception->getCode(),
                $exception->getFile(),
                $exception->getLine()
            ),
            [
                'trace' => $exception->getTrace(),
                'code' => $httpStatusCode
            ]
        );

        // Content negotiation
        if ('application/json' === $request->getHeaderLine('Accept') || (!empty($_GET['format']) && $_GET['format'] === 'json')) {
            $response = $response->withAddedHeader('Content-Type', 'application/json');

            // JSON
            return $this->renderJson($exception, $response);
        }

        // HTML
        return $this->renderHtml($exception, $response);
    }

    /**
     * getHttpStatusCode
     *
     * Normalize the exception code to an HTTP status code so we can deliver
     * the a response with the correct code.
     *
     * @param Throwable $exception
     * @return integer
     */
    private function getHttpStatusCode(Throwable $exception): int
    {
        // $statusCode = StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR;

        // if ($exception instanceof \Slim\Exception\HttpException || $exception instanceof \Symfony\Component\HttpKernel\Exception\HttpException) {
        //     var_dump($exception);
        //     die();
        // }

        // if ($exception instanceof DomainException || $exception instanceof InvalidArgumentException) {
        //     $statusCode = StatusCodeInterface::STATUS_BAD_REQUEST;
        // }
        $statusCode = $exception->getCode();
        if ($statusCode < 400 || $statusCode > 599) {
            $statusCode = 401;
        }
        return $statusCode;
    }

    private function renderJson(Throwable $exception, ResponseInterface $response): ResponseInterface
    {
        $data = [
            'error' => [
                'message' => $exception->getMessage(),
            ]
        ];
        if ($this->displayErrorDetails) {
            $data['error']['details'] = $exception->getTrace();
        }
        $data['errors'] = $this->errors;
        return $this->jsonRenderer->json($response, $data);
    }

    private function renderHtml(Throwable $exception, ResponseInterface $response): ResponseInterface
    {
        $type = explode('\\', get_class($exception));
        $type = end($type);

        return $this->twig->render($response, 'error.html.twig', [
            'type' => $type,
            'error' => $exception,
            'errors' => $this->errors
        ]);
    }
}
