<?php

declare(strict_types=1);

namespace App\Action\Event;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Event\Service\EditEventService;
use App\Domain\Event\Service\FetchEventService;
use App\Domain\Role\Service\FetchRolesService;
use App\Domain\Visit\Service\FetchVisitService;
use DI\Attribute\Inject;
use Psr\Http\Message\ResponseInterface;

class EditEventAction extends Action implements ActionInterface
{
    #[Inject()]
    private FetchEventService $eventService;

    #[Inject()]
    private EditEventService $editService;

    #[Inject()]
    private FetchRolesService $roleService;

    #[Inject()]
    private FetchVisitService $visitService;

    public function action(): ResponseInterface
    {
        $event = $this->eventService->getEventById(
            (int) $this->getArg('id'),
            true
        );
        if ($this->isPost()) {
            $data = $this->getRequest()->getParsedBody();
            $id = $this->editService->updateEvent(
                event: $event,
                start: $data['start'],
                end: $data['end'],
                name: $data['name'],
                desc: $data['desc'],
                roles: (!empty($data['roles'])) ? $data['roles'] : [],
                editor: $this->getUser(),
                timezone: $data['timezone']
            );
            return $this->redirectFor('event.view', ['id' => (int) $id]);
        }
        return $this->render('event/edit.html.twig', [
            'event' => $event,
            'roles' => $this->roleService->getRoles(),
            'visits' => $this->visitService->getVisitsForEvent($event)
        ]);
    }
}
