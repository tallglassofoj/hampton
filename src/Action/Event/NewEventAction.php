<?php

declare(strict_types=1);

namespace App\Action\Event;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Event\Service\CreateEventService;
use App\Domain\Role\Service\FetchRolesService;
use DI\Attribute\Inject;
use Psr\Http\Message\ResponseInterface;

class NewEventAction extends Action implements ActionInterface
{
    #[Inject()]
    private CreateEventService $eventService;

    #[Inject()]
    private FetchRolesService $rolesService;

    public function action(): ResponseInterface
    {
        // if (!$this->getUser()->has(Permissions::SCHEDULE_EVENTS)) {
        //     throw new HttpForbiddenException($this->getRequest(), "You do not have permission to perform this action");
        // }
        if ($this->isPost()) {
            $data = $this->getRequest()->getParsedBody();
            if (empty($data['roles'])) {
                $data['roles'] = [];
            }
            $id = $this->eventService->createNewEvent(
                start: $data['start'],
                end: $data['end'],
                name: $data['name'],
                desc: $data['desc'],
                type: $data['type'],
                roles: $data['roles'],
                creator: $this->getUser(),
                timezone: $data['timezone']
            );
            return $this->redirectFor('event.view', ['id' => (int) $id]);
        }

        return $this->render('event/new.html.twig', [
            'roles' => $this->rolesService->getRoles(),
            'auth' => [
                'action' => 'Schedule A New Event',
                'status' => 'authorized',
                'source' => 'No Checks In Progress'
            ]
        ]);
    }
}
