<?php

declare(strict_types=1);

namespace App\Action\Event;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Event\Service\CreateEventService;
use App\Domain\Event\Service\FetchEventService;
use DI\Attribute\Inject;
use Psr\Http\Message\ResponseInterface;

class ListEventsAction extends Action implements ActionInterface
{
    #[Inject()]
    private FetchEventService $eventService;

    public function action(): ResponseInterface
    {
        $events = $this->eventService->getUpcomingEvents();
        return $this->render('event/listing.html.twig', [
            'events' => $events
        ]);
    }
}
