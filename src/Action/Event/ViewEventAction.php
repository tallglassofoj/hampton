<?php

declare(strict_types=1);

namespace App\Action\Event;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Event\Service\CreateEventService;
use App\Domain\Event\Service\FetchEventService;
use App\Domain\Role\Service\FetchRolesService;
use App\Domain\Visit\Service\FetchVisitService;
use DI\Attribute\Inject;
use Psr\Http\Message\ResponseInterface;

class ViewEventAction extends Action implements ActionInterface
{
    #[Inject()]
    private FetchEventService $eventService;

    #[Inject()]
    private FetchRolesService $roleService;

    #[Inject()]
    private FetchVisitService $visitService;

    public function action(): ResponseInterface
    {
        $event = $this->eventService->getEventById((int) $this->getArg('id'));
        return $this->render('event/single.html.twig', [
            'event' => $event,
            'roles' => $this->roleService->getRolesForEvent($event),
            'visits' => $this->visitService->getVisitsForEvent($event)
        ]);
    }
}
