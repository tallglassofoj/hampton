<?php

declare(strict_types=1);

namespace App\Action\Guard;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Guard\Service\CreateGuardProfileService;
use DI\Attribute\Inject;
use Psr\Http\Message\ResponseInterface;

class CreateGuardProfileAction extends Action implements ActionInterface
{
    #[Inject()]
    private CreateGuardProfileService $profile;

    public function action(): ResponseInterface
    {
        if($this->isPost()) {
            $data = $this->request->getParsedBody();
            $this->profile->createGuard(
                $data['firstName'],
                $data['lastName'],
                $data['alias'],
                $this->getUser()
            );
            return $this->redirectFor('home');
        }
        return $this->render('guard/new.html.twig');
    }

}
