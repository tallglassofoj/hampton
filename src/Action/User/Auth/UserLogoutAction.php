<?php

declare(strict_types=1);

namespace App\Action\User\Auth;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\User\Service\DeauthenticateUserService;
use DI\Attribute\Inject;
use Psr\Http\Message\ResponseInterface;

class UserLogoutAction extends Action implements ActionInterface
{
    #[Inject()]
    private DeauthenticateUserService $deauth;

    public function action(): ResponseInterface
    {
        $this->deauth->deauthenticateUser();
        return $this->redirectFor('home');
    }

}
