<?php

declare(strict_types=1);

namespace App\Action\User\Auth;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\User\Service\AuthenticateUserService;
use App\Exception\InvalidLoginCredentials;
use DI\Attribute\Inject;
use Exception;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpUnauthorizedException;

class UserLoginAction extends Action implements ActionInterface
{
    #[Inject]
    private AuthenticateUserService $auth;

    public function action(): Response
    {
        if ('POST' === $this->request->getMethod()) {
            try {
                $data = $this->request->getParsedBody();
                $user = $this->auth->authenticateUserWithPassword(
                    $data['email'],
                    $data['password']
                );
                return $this->redirectFor('home');
            } catch (Exception $e) {
                throw new InvalidLoginCredentials("Invalid email or password provided");
            }
        }
        return $this->render('guest/login.html.twig');
    }
}
