<?php

declare(strict_types=1);

namespace App\Action\User\Auth;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\User\Service\CreateUserService;
use DI\Attribute\Inject;
use Nyholm\Psr7\Response;

class UserRegistrationAction extends Action implements ActionInterface
{
    #[Inject]
    private CreateUserService $createUser;

    public function action(): Response
    {
        if ('POST' === $this->request->getMethod()) {
            $data = $this->request->getParsedBody();
            $this->createUser->createNewUserFromRequest(
                firstName: $data['firstName'],
                lastName: $data['lastName'],
                email: $data['email'],
                password: $data['password']
            );
            return $this->redirectFor('home');
        }
        return $this->render('guest/register.html.twig');
    }
}
