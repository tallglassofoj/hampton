<?php

declare(strict_types=1);

namespace App\Action\User\Auth;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\User\Service\AuthenticateUserService;
use DI\Attribute\Inject;
use Nyholm\Psr7\Response;

class UserSudoModeAction extends Action implements ActionInterface
{
    #[Inject]
    private AuthenticateUserService $auth;

    public function action(): Response
    {
        if ('POST' === $this->request->getMethod()) {
            $data = $this->request->getParsedBody();
            $this->auth->authenticateUserWithPassword(
                $data['email'],
                $data['password']
            );
            return $this->redirectFor('home');
        }
        return $this->render('guest/login.html.twig');
    }
}
