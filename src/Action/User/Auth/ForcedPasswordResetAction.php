<?php

declare(strict_types=1);

namespace App\Action\User\Auth;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\User\Service\ResetPasswordService;
use DI\Attribute\Inject;
use Exception;
use Nyholm\Psr7\Response;
use Slim\Exception\HttpUnauthorizedException;

class ForcedPasswordResetAction extends Action implements ActionInterface
{
    #[Inject]
    private ResetPasswordService $passwordService;

    public function action(): Response
    {

        if ($this->isPost()) {
            try {
                $data = $this->request->getParsedBody();
                $this->passwordService->setNewPassword(
                    $this->getUser(),
                    $data['password'],
                    $data['password2']
                );
                return $this->redirectFor('home');
            } catch (Exception $e) {
                throw new HttpUnauthorizedException(
                    $this->getRequest(),
                    "Your password could not be changed. Please try again."
                );
            }
        }
        return $this->render('user/forcePasswordReset.html.twig');
    }
}
