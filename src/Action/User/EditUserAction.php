<?php

declare(strict_types=1);


namespace App\Action\User;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Profile\Service\CreateProfileService;
use App\Domain\Profile\Service\FetchUserProfilesService;
use App\Domain\Rank\Service\FindRankService;
use App\Domain\Role\Service\FetchRolesService;
use App\Domain\User\Service\EditUserService;
use App\Domain\User\Service\FetchUserService;
use DI\Attribute\Inject;
use Psr\Http\Message\ResponseInterface;

class EditUserAction extends Action implements ActionInterface
{

    #[Inject()]
    private FetchUserService $userService;

    #[Inject()]
    private EditUserService $editService;

    #[Inject()]
    private FetchUserProfilesService $profileService;

    #[Inject()]
    private FindRankService $rankService;

    #[Inject()]
    private FetchRolesService $roleService;

    #[Inject()]
    private CreateProfileService $createProfileService;


    public function action(): ResponseInterface
    {
        $id = (int) $this->getArg('user');
        $user = $this->userService->fetchUserById($id);
        $user->setProfiles($this->profileService->getProfilesForUser($user));
        $ranks = $this->rankService->listRanks();
        if ($this->isPost()) {
            if ($action = $this->getArg('action')) {
                $data = $this->getRequest()->getParsedBody();
                match ($action) {
                    'activate' => $this->editService->toggleUser($user),
                    'rank' => $this->editService->setRank(
                        $user,
                        (int) $data['rank']
                    ),
                    'profile' => $this->createProfileService->createProfile(
                        $data['firstName'],
                        $data['lastName'],
                        $data['alias'],
                        $user,
                        (int) $data['role']
                    ),
                    'password' => $this->editService->setPassword(
                        $user,
                        $data['password'],
                        $data['password2']
                    ),
                };
            }

            return $this->redirectFor('user.edit', ['user' => $user->getId()]);
        }
        return $this->render('user/single.html.twig', [
            'user' => $user,
            'ranks' => $ranks,
            'roles' => $this->roleService->getRoles()
        ]);
    }
}
