<?php

declare(strict_types=1);


namespace App\Action\User;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\User\Service\FetchUserService;
use DI\Attribute\Inject;
use Psr\Http\Message\ResponseInterface;

class ListUsersAction extends Action implements ActionInterface
{

    #[Inject()]
    private FetchUserService $userService;

    public function action(): ResponseInterface
    {
        return $this->render('user/listing.html.twig', [
            'users' => $this->userService->fetchAllUsers()
        ]);
    }
}
