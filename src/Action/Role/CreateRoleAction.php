<?php

declare(strict_types=1);

namespace App\Action\Role;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Role\Service\CreateRoleService;
use DI\Attribute\Inject;
use Psr\Http\Message\ResponseInterface;

class CreateRoleAction extends Action implements ActionInterface
{
    #[Inject()]
    private CreateRoleService $roleService;

    public function action(): ResponseInterface
    {
        if ($this->isPost()) {
            $data = $this->getRequest()->getParsedBody();
            $id = $this->roleService->createNewRole(
                $data['name'],
                $data['color'],
                $data['icon'],
                $this->getUser()
            );
            return $this->redirectFor('role.view', ['role' => $id]);
        }
        return $this->render('role/newRole.html.twig');
    }
}
