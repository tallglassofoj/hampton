<?php

declare(strict_types=1);

namespace App\Action\Role;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Role\Service\CreateRoleService;
use App\Domain\Role\Service\FetchRolesService;
use DI\Attribute\Inject;
use Psr\Http\Message\ResponseInterface;

class ListRolesAction extends Action implements ActionInterface
{
    #[Inject()]
    private FetchRolesService $roleService;

    public function action(): ResponseInterface
    {
        $roles = $this->roleService->getRoles();
        return $this->render('role/listing.html.twig', [
            'roles' => $roles
        ]);
    }
}
