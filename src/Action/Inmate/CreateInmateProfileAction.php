<?php

declare(strict_types=1);

namespace App\Action\Inmate;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Inmate\Service\CreateInmateProfileService;
use DI\Attribute\Inject;
use Psr\Http\Message\ResponseInterface;

class CreateInmateProfileAction extends Action implements ActionInterface
{
    #[Inject()]
    private CreateInmateProfileService $profile;

    public function action(): ResponseInterface
    {
        if($this->isPost()) {
            $data = $this->request->getParsedBody();
            $this->profile->createInmate(
                $data['firstName'],
                $data['lastName'],
                $data['alias'],
                $this->getUser()
            );
            return $this->redirectFor('home');
        }
        return $this->render('inmate/new.html.twig');
    }

}
