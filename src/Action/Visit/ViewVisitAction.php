<?php

declare(strict_types=1);

namespace App\Action\Visit;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Event\Service\FetchEventService;
use App\Domain\Metadata\Visit\Service\FetchVisitMetadataService;
use App\Domain\Profile\Service\FetchProfileService;
use App\Domain\Visit\Service\FetchVisitService;
use DI\Attribute\Inject;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpNotFoundException;

class ViewVisitAction extends Action implements ActionInterface
{
    #[Inject()]
    private FetchVisitService $visitService;

    #[Inject()]
    private FetchEventService $eventService;

    #[Inject()]
    private FetchProfileService $profileService;

    #[Inject()]
    private FetchVisitMetadataService $metadataService;

    public function action(): ResponseInterface
    {
        $visit = $this->visitService->getVisitById((int) $this->getArg('visit'));

        if(!$visit) {
            throw new HttpNotFoundException(
                $this->request,
                "The specified visit could not be located",
                null
            );
        }

        $event = $this->eventService->getEventById($visit->getEvent());

        $profile = $this->profileService->getProfileById($visit->getProfile());

        $metadata = $this->metadataService->getMetadataForVisit($visit);

        return $this->render('visit/single.html.twig', [
            'visit' => $visit,
            'event' => $event,
            'profile' => $profile,
            'metadata' => $metadata
        ]);
    }
}
