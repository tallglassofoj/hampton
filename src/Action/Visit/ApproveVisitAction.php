<?php

declare(strict_types=1);

namespace App\Action\Visit;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Event\Service\FetchEventService;
use App\Domain\Metadata\Visit\Service\FetchVisitMetadataService;
use App\Domain\Profile\Service\FetchProfileService;
use App\Domain\Visit\Service\FetchVisitService;
use App\Domain\Visit\Service\UpdateVisitService;
use DateTimeImmutable;
use DI\Attribute\Inject;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpNotFoundException;

class ApproveVisitAction extends Action implements ActionInterface
{
    #[Inject()]
    private FetchVisitService $visitService;

    #[Inject()]
    private UpdateVisitService $updateService;

    public function action(): ResponseInterface
    {
        $visit = $this->visitService->getVisitById((int) $this->getArg('visit'));

        if(!$visit) {
            throw new HttpNotFoundException(
                $this->request,
                "The specified visit could not be located",
                null
            );
        }

        $this->updateService->approveVisit(
            $visit,
            $this->getUser()
        );
        return $this->redirectFor(
            'visit.view',
            ['visit' => $visit->getId()]
        );

    }
}
