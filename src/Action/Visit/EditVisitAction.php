<?php

declare(strict_types=1);

namespace App\Action\Visit;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Event\Service\FetchEventService;
use App\Domain\Metadata\Visit\Service\FetchVisitMetadataService;
use App\Domain\Profile\Service\FetchProfileService;
use App\Domain\User\Data\Permissions;
use App\Domain\Visit\Service\FetchVisitService;
use App\Domain\Visit\Service\UpdateVisitService;
use DateTime;
use DateTimeImmutable;
use DI\Attribute\Inject;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpForbiddenException;
use Slim\Exception\HttpNotFoundException;

class EditVisitAction extends Action implements ActionInterface
{
    #[Inject()]
    private FetchVisitService $visitService;

    #[Inject()]
    private UpdateVisitService $updateService;

    #[Inject()]
    private FetchEventService $eventService;

    #[Inject()]
    private FetchProfileService $profileService;

    #[Inject()]
    private FetchVisitMetadataService $metadataService;

    public function action(): ResponseInterface
    {
        $visit = $this->visitService->getVisitById((int) $this->getArg('visit'));

        if (!$visit) {
            throw new HttpNotFoundException(
                $this->request,
                "The specified visit could not be located",
                null
            );
        }

        if ($visit->isApproved() && !$this->getUser()->has(Permissions::SCHEDULE_EVENTS)) {
            throw new HttpForbiddenException(
                $this->request,
                "You cannot modify an approved visit",
                null
            );
        }

        if ($this->isPost()) {
            $data = $this->getRequest()->getParsedBody();

            $this->updateService->updateVisit(
                visit: $visit,
                user: $this->getUser(),
                start: $data['start'],
                end: $data['end'],
                timezone: $data['timezone']
            );
            return $this->redirectFor(
                'visit.view',
                ['visit' => $visit->getId()]
            );
        }
        $event = $this->eventService->getEventById($visit->getEvent());

        $profile = $this->profileService->getProfileById($visit->getProfile());

        $metadata = $this->metadataService->getMetadataForVisit($visit);

        return $this->render('visit/edit.html.twig', [
            'visit' => $visit,
            'event' => $event,
            'profile' => $profile,
            'metadata' => $metadata
        ]);
    }
}
