<?php

declare(strict_types=1);

namespace App\Action\Visit;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Event\Service\FetchEventService;
use App\Domain\Visit\Service\CreateVisitService;
use DI\Attribute\Inject;
use Exception;
use Psr\Http\Message\ResponseInterface;

class RequestShiftAction extends Action implements ActionInterface
{
    #[Inject()]
    private CreateVisitService $visitService;

    #[Inject()]
    private FetchEventService $eventService;

    public function action(): ResponseInterface
    {
        if (!$this->getUser()->getGuard()) {
            throw new Exception("You need a guard profile before you can request a shift");
        }

        $event = $this->eventService->getEventById((int) $this->getArg('id'));
        if ($this->isPost()) {
            $id = $this->visitService->createNewShift(
                $event,
                $this->getUser(),
                $this->getRequest()->getParsedBody()
            );
            return $this->redirectFor('visit.view', ['visit' => $id]);
        }

        return $this->render('visit/requestShift.html.twig', [
            'event' => $event
        ]);
    }
}
