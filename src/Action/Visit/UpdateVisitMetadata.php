<?php

declare(strict_types=1);

namespace App\Action\Visit;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Metadata\Visit\Data\MetadataKeys;
use App\Domain\Metadata\Visit\Service\UpdateVisitMetadataService;
use App\Domain\Visit\Service\FetchVisitService;
use DI\Attribute\Inject;
use Psr\Http\Message\ResponseInterface;

class UpdateVisitMetadata extends Action implements ActionInterface
{
    #[Inject()]
    private FetchVisitService $visitService;

    #[Inject()]
    private UpdateVisitMetadataService $metadataService;

    public function action(): ResponseInterface
    {
        $key = MetadataKeys::tryFrom($this->getArg('key'));
        $visit = $this->visitService->getVisitById((int) $this->getArg('visit'));
        if($this->isPost()) {
            $data = $this->request->getParsedBody();
            $data['key'] = $key;
            $this->metadataService->updateMetadata($this->request->getParsedBody(), $key, $visit);
        }
        return $this->redirectFor('visit.view', ['visit' => $visit->getId()]);
    }

}
