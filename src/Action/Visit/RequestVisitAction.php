<?php

declare(strict_types=1);

namespace App\Action\Visit;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Event\Service\FetchEventService;
use App\Domain\Visit\Service\CreateVisitService;
use DI\Attribute\Inject;
use Exception;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpBadRequestException;

class RequestVisitAction extends Action implements ActionInterface
{
    #[Inject()]
    private CreateVisitService $visitService;

    #[Inject()]
    private FetchEventService $eventService;

    public function action(): ResponseInterface
    {

        $event = $this->eventService->getEventById((int) $this->getArg('id'));
        // if ($this->isPost()) {
        //     $id = $this->visitService->createNewShift(
        //         $event,
        //         $this->getUser(),
        //         $this->getRequest()->getParsedBody()
        //     );
        //     return $this->redirectFor('visit.view', ['visit' => $id]);
        // }
        $role = (int) $this->getArg('profile');
        if (!$profile = $this->getUser()->hasProfileForRole($role)) {
            throw new HttpBadRequestException($this->request, "Invalid profile specified");;
        }
        return $this->render('visit/requestVisit.html.twig', [
            'event' => $event,
            'profile' => $profile
        ]);
    }
}
