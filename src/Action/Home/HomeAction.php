<?php

namespace App\Action\Home;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Visit\Service\FetchVisitService;
use DI\Attribute\Inject;
use Nyholm\Psr7\Response;

final class HomeAction extends Action implements ActionInterface
{
    #[Inject()]
    private FetchVisitService $visitService;

    public function action(): Response
    {
        $visits = null;
        if($this->getUser()) {
            $visits = $this->visitService->getVisitsForUser($this->getUser());
        }
        return $this->render('index.html.twig', [
            'visits' => $visits
        ]);
    }
}
