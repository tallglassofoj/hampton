<?php

namespace App\Action\Calendar;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Visit\Service\FetchVisitService;
use DI\Attribute\Inject;
use Nyholm\Psr7\Response;

final class CalendarAction extends Action implements ActionInterface
{

    public function action(): Response
    {
        return $this->render('calendar.html.twig');
    }
}
