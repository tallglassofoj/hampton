<?php

namespace App\Action\Calendar;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Event\Service\FetchEventService;
use App\Domain\User\Data\Permissions;
use App\Domain\Visit\Service\FetchVisitService;
use DateTimeImmutable;
use DI\Attribute\Inject;
use Nyholm\Psr7\Response;

final class GetEventsAction extends Action implements ActionInterface
{

    #[Inject()]
    private FetchEventService $eventService;

    #[Inject()]
    private FetchVisitService $visitService;

    public function action(): Response
    {
        $start = new DateTimeImmutable($_GET['start']);
        $end = new DateTimeImmutable($_GET['end']);
        $start->setTime(0, 0, 0);
        $end->setTime(23, 59, 59);
        $events = $this->eventService->findEventsBetween($start, $end);
        $visits = $this->visitService->findVisitsBetween($start, $end);
        foreach ($events as &$e) {
            $e = $e->getSimpleCalendarEntry();
        }
        foreach ($visits as &$v) {
            $v = $v->getSimpleCalendarEntry();
            if (!$this->getUser()->has(Permissions::VIEW_DETAILS)) {
                $v['title'] = $v['type'];
            }
        }
        return $this->json([...$events, ...$visits]);
    }
}
