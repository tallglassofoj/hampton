<?php

declare(strict_types=1);


namespace App\Action\Rank;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Rank\Service\CreateRankService;
use DI\Attribute\Inject;
use Psr\Http\Message\ResponseInterface;

class CreateRankAction extends Action implements ActionInterface
{

    #[Inject()]
    private CreateRankService $rankService;

    public function action(): ResponseInterface
    {
        $data = $this->request->getParsedBody();
        $this->rankService->createNewRank($data['name'], array_sum($data['flags']));
        return $this->redirectFor('ranks.list');
    }
}
