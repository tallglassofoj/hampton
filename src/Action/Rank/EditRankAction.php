<?php

declare(strict_types=1);


namespace App\Action\Rank;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Rank\Service\CreateRankService;
use App\Domain\Rank\Service\EditRankService;
use App\Domain\Rank\Service\FindRankService;
use DI\Attribute\Inject;
use Psr\Http\Message\ResponseInterface;

class EditRankAction extends Action implements ActionInterface
{

    #[Inject()]
    private EditRankService $rankEdit;

    #[Inject()]
    private FindRankService $rankFinder;

    public function action(): ResponseInterface
    {
        $id = (int) $this->getArg('id');
        $rank = $this->rankFinder->getRankById($id);
        if ($this->isPost()) {
            $data = $this->request->getParsedBody();
            $this->rankEdit->editRank($data['name'], array_sum($data['flags']), $rank);
            return $this->redirectFor('ranks.edit', ['id' => $rank->getId()]);
        }
        return $this->render('rank/edit.html.twig', [
            'rank' => $rank
        ]);
    }
}
