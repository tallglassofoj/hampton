<?php

declare(strict_types=1);


namespace App\Action\Rank;

use App\Action\Action;
use App\Action\ActionInterface;
use App\Domain\Rank\Service\FindRankService;
use DI\Attribute\Inject;
use Psr\Http\Message\ResponseInterface;

class ListRanksAction extends Action implements ActionInterface
{

    #[Inject()]
    private FindRankService $rankService;

    public function action(): ResponseInterface
    {
        return $this->render('rank/listing.html.twig', [
            'ranks' => $this->rankService->listRanks()
        ]);
    }
}
