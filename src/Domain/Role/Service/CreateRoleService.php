<?php

declare(strict_types=1);

namespace App\Domain\Role\Service;

use App\Domain\Role\Repository\RoleRepository;
use App\Domain\User\Entity\User;
use App\Service\FlashMessageService;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;

class CreateRoleService
{
    public function __construct(
        private RoleRepository $roleRepository,
        private FlashMessageService $flash
    ) {

    }

    public function createNewRole(
        string $name,
        string $color,
        string $icon,
        User $creator
    ): int {
        try {
            $this->flash->addSuccessMessage("Role created successfully");
            return $this->roleRepository->insertNewRole(
                name: $name,
                color: $color,
                icon: $icon,
                creator: $creator
            );
        } catch (UniqueConstraintViolationException $e) {
            throw new Exception("This role already exists");
        }
    }

}
