<?php

declare(strict_types=1);

namespace App\Domain\Role\Service;

use App\Domain\Event\Data\EventTypeEnum;
use App\Domain\Event\Entity\Event;
use App\Domain\Guard\Entity\Guard;
use App\Domain\Inmate\Entity\Inmate;
use App\Domain\Profile\Data\ProfileType;
use App\Domain\Role\Entity\Role;
use App\Domain\Role\Repository\RoleRepository;

class FetchRolesService
{

    public function __construct(
        private RoleRepository $roleRepository
    ) {
    }

    public function getRoles(): array
    {
        $roles = [
            ...$this->getBuiltinRoles(),
            ...$this->roleRepository->getRoles()
        ];
        return $roles;
    }

    /**
     * getRolesForEvent
     *
     * @param Event $event
     * @return array<Event>
     */
    public function getRolesForEvent(Event $event): array
    {
        $roles = $this->roleRepository->getRolesForEvent($event);
        if (EventTypeEnum::RP === $event->getType()) {
            $roles = [...$this->getBuiltinRoles(), ...$roles];
        }
        return array_unique($roles);
    }

    public function getRoleById(int $id): Role
    {
        return $this->roleRepository->findOneBy('id', $id);
    }

    private function getBuiltinRoles(): array
    {
        return [
            self::getInmateRole(),
            self::getGuardRole()
        ];
    }

    public static function getInmateRole(): Role
    {
        return new Role(
            id: null,
            type: ProfileType::INMATE,
            icon: Inmate::INMATE_ICON,
            color: Inmate::INMATE_COLOR,
            name: 'Inmate',
            visitName: Inmate::VISIT_NAME,
            visitRoute: Inmate::VISIT_ROUTE,
            foreColor: '#000'
        );
    }

    public static function getGuardRole(): Role
    {
        return new Role(
            id: null,
            type: ProfileType::GUARD,
            icon: Guard::GUARD_ICON,
            color: Guard::GUARD_COLOR,
            name: 'Guard',
            visitName: Guard::VISIT_NAME,
            visitRoute: Guard::VISIT_ROUTE
        );
    }
}
