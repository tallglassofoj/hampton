<?php

declare(strict_types=1);

namespace App\Domain\Role\Entity;

use App\Domain\Inmate\Entity\Inmate;
use App\Domain\Profile\Data\ProfileType;
use App\Domain\Role\Service\FetchRolesService;
use App\Service\LuminosityContrast;

class Role
{
    public function __construct(
        private ?int $id,
        private string $icon,
        private string $color,
        private string $name,
        private ProfileType $type = ProfileType::CUSTOM,
        private string $visitName = 'Visit',
        private string $visitRoute = 'visit.request',
        private ?string $foreColor = null
    ) {
    }

    public static function new($data): static
    {
        if (!empty($data->role_type)) {
            if ($data->role_type === ProfileType::INMATE->value) {
                return FetchRolesService::getInmateRole();
            }
            if ($data->role_type === ProfileType::GUARD->value) {
                return FetchRolesService::getGuardRole();
            }
        }
        return new self(
            id: $data->id,
            icon: $data->icon,
            color: $data->color,
            name: $data->name,
            foreColor: $data->foreColor
        );
    }

    public function getIcon(): string
    {
        return $this->icon;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function getForeColor(): string
    {
        if (!$this->foreColor) {
            return LuminosityContrast::getContrastColor($this->getColor());
        }
        return $this->foreColor;
    }

    public function getStyleString(): string
    {
        return sprintf(
            'background-color: %s; color: %s',
            $this->getColor(),
            $this->getForeColor()
        );
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): ProfileType
    {
        return $this->type;
    }

    public function getVisitName(): string
    {
        return $this->visitName;
    }

    public function getVisitRoute(): string
    {
        return $this->visitRoute;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return $this->getName();
    }
}
