<?php

declare(strict_types=1);

namespace App\Domain\Role\Repository;

use App\Domain\Event\Entity\Event;
use App\Domain\Role\Entity\Role;
use App\Domain\User\Entity\User;
use App\Repository\BaseQuery;
use App\Repository\Repository;
use Doctrine\DBAL\Query\QueryBuilder;

class RoleRepository extends Repository implements BaseQuery
{
    public const TABLE = 'role';
    public const ALIAS = 'r';

    public const COLUMNS = [
        'r.id',
        'r.name',
        'r.color',
        'r.icon',
        'r.created',
        'r.creator',
        'r.foreColor'
    ];

    public ?string $entityClass = Role::class;

    public function getBaseQuery(): QueryBuilder
    {
        $qb = $this->qb();
        $qb->select(...self::COLUMNS)
            ->from(self::TABLE, self::ALIAS);
        return $qb;
    }

    public function insertNewRole(
        string $name,
        string $color,
        string $icon,
        User $creator,
    ): int {
        $qb = $this->qb();
        $qb->insert(self::TABLE)
            ->values([
                'name' => $qb->createNamedParameter($name),
                'color' => $qb->createNamedParameter($color),
                'icon' => $qb->createNamedParameter($icon),
                'creator' => $qb->createNamedParameter($creator->getId())
            ])->executeStatement();
        return (int) $this->connection->lastInsertId();
    }

    public function getRoles(): array
    {
        $qb = $this->getBaseQuery();
        return $this->getResults($qb->executeQuery());
    }

    public function getRolesForEvent(Event $event): array
    {

        $qb = $this->qb();
        $qb->select('e.role_type', ...self::COLUMNS);
        $qb->from('event_roles', 'e')
            ->where("e.event = " . $qb->createNamedParameter($event->getId()))
            ->leftJoin('e', self::TABLE, self::ALIAS, 'e.role = r.id');

        return $this->getResults($qb->executeQuery());
    }

    public function findOneBy(string $field, string|int $value): ?Role
    {
        $qb = $this->qb();
        $qb->select(...self::COLUMNS);
        $qb->from(self::TABLE, self::ALIAS);
        $qb->where(self::ALIAS . ".$field = " . $qb->createNamedParameter($value));
        return $this->getResult($qb->executeQuery());
    }
}
