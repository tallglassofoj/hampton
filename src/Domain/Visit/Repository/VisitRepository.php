<?php

declare(strict_types=1);

namespace App\Domain\Visit\Repository;

use App\Domain\Event\Entity\Event;
use App\Domain\Event\Entity\EventVisits;
use App\Domain\Event\Repository\EventRepository;
use App\Domain\Profile\Entity\Profile;
use App\Domain\Profile\Repository\ProfileRepository;
use App\Domain\User\Entity\User;
use App\Domain\User\Entity\UserVisits;
use App\Domain\Visit\Entity\Visit;
use App\Repository\BaseQuery;
use App\Repository\Repository;
use DateTimeImmutable;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\SqlFormatter\CliHighlighter;
use Doctrine\SqlFormatter\NullHighlighter;
use Doctrine\SqlFormatter\SqlFormatter;

class VisitRepository extends Repository implements BaseQuery
{
    public const COLUMNS = [
        'v.id',
        'v.event',
        'v.profile',
        'v.approved',
        'v.start',
        'v.end',
        'v.edited',
        'IFNULL(v.start, e.start) AS `start`',
        'IFNULL(v.end, e.end) AS `end`',
    ];

    public const TABLE = 'visit';
    public const ALIAS = 'v';

    public ?string $entityClass = Visit::class;

    public function getBaseQuery(): QueryBuilder
    {
        $qb = $this->qb();
        $qb->select(...self::COLUMNS)
            ->leftJoin(self::ALIAS, EventRepository::TABLE, EventRepository::ALIAS, 'e.id = v.event')
            ->from(self::TABLE, self::ALIAS);
        return $qb;
    }

    public function getVisitById(int $id): Visit
    {
        $qb = $this->getBaseQuery();
        $qb->where('v.id = ' . $qb->createNamedParameter($id));
        $result = $qb->executeQuery();
        return $this->getResult($result);
    }

    public function insertVisit(
        Event $event,
        Profile $profile,
        User $user
    ): int {
        $qb = $this->qb();
        $qb->insert(self::TABLE)
            ->values([
                'event' => $qb->createNamedParameter($event->getId()),
                'profile' => $qb->createNamedParameter($profile->getId()),
                'creator' => $qb->createNamedParameter($user->getId())
            ])->executeStatement();
        return (int) $this->connection->lastInsertId();
    }

    public function updateVisit(
        Visit $visit,
        User $editor,
        DateTimeImmutable $start,
        DateTimeImmutable $end
    ): void {
        $qb = $this->qb();
        $qb->update(self::TABLE)
            ->set(
                'start',
                $qb->createNamedParameter($start->format('Y-m-d H:i:s'))
            )
            ->set(
                'end',
                $qb->createNamedParameter($end->format('Y-m-d H:i:s'))
            )->set(
                'editor',
                $qb->createNamedParameter($editor->getId())
            )
            ->where('id = ' . $qb->createNamedParameter($visit->getId()))
            ->executeStatement();
    }

    public function approveVisit(
        Visit $visit,
        User $approver
    ): void {
        $qb = $this->qb();
        $qb->update(self::TABLE)
            ->set(
                'approved',
                '1'
            )->set(
                'approver',
                $qb->createNamedParameter($approver->getId())
            )
            ->where('id = ' . $qb->createNamedParameter($visit->getId()))
            ->executeStatement();
    }

    public function disapproveVisit(
        Visit $visit,
        User $approver
    ): void {
        $qb = $this->qb();
        $qb->update(self::TABLE)
            ->set(
                'approved',
                '0'
            )->set(
                'approver',
                $qb->createNamedParameter($approver->getId())
            )
            ->where('id = ' . $qb->createNamedParameter($visit->getId()))
            ->executeStatement();
    }

    public function findOneBy(string $field, string|int $value): ?Visit
    {
        $qb = $this->getBaseQuery();
        $qb->where(self::ALIAS . ".$field = " . $qb->createNamedParameter($value));
        return $this->getResult($qb->executeQuery());
    }

    public function findVisitsForUser(User $user): array
    {
        $cols = [
            'v.id',
            'v.start as vStart',
            'v.end as vEnd',
            'v.approved',
            'v.profile',
            'e.name',
            'e.start as eStart',
            'e.end as eEnd',
            'e.type',
            'p.id as pId',
            'p.firstName',
            'p.lastName',
            'p.alias',
            'p.type as pType',
            'p.roleId',
            'p.user as userId',
            'r.color',
            'r.foreColor',
            'r.icon',
            'r.name as roleName',
            'IFNULL(v.start, e.start) AS `start`',
            'IFNULL(v.end, e.end) AS `end`',
        ];
        $qb = $this->qb();
        $qb->select(...$cols)
            ->from(
                ProfileRepository::TABLE,
                ProfileRepository::ALIAS
            )
            ->leftJoin(
                ProfileRepository::ALIAS,
                self::TABLE,
                self::ALIAS,
                'p.id = v.profile'
            )
            ->leftJoin(
                self::ALIAS,
                EventRepository::TABLE,
                EventRepository::ALIAS,
                'e.id = v.event'
            )
            ->leftJoin(
                'p',
                'role',
                'r',
                'r.id = p.roleId'
            )
            ->where(
                'p.user = ' . $qb->createNamedParameter($user->getId()),
                'v.id IS NOT NULL'
            )->addGroupBy(
                'v.id'
            );
        return $this->getResults($qb->executeQuery(), UserVisits::class);
    }

    public function findVisitsForEvent(Event $event): array
    {
        $cols = [
            'v.id',
            'v.start as vStart',
            'v.end as vEnd',
            'v.approved',
            'v.event',
            'p.id as pId',
            'p.firstName',
            'p.lastName',
            'p.alias',
            'p.type',
            'p.roleId',
            'r.color',
            'r.foreColor',
            'r.icon',
            'r.name as roleName'
        ];
        $qb = $this->qb();
        $qb->select(...$cols)
            ->from(
                self::TABLE,
                self::ALIAS
            )
            ->leftJoin(
                self::ALIAS,
                ProfileRepository::TABLE,
                ProfileRepository::ALIAS,
                'p.id = v.profile'
            )
            ->leftJoin(
                'p',
                'role',
                'r',
                'r.id = p.roleId'
            )
            ->where(
                'v.id IS NOT NULL',
                'v.event = ' . $qb->createNamedParameter($event->getId())
            )->addGroupBy(
                'v.id'
            );
        return $this->getResults($qb->executeQuery(), EventVisits::class);
    }

    public function findVisitsBetween(DateTimeImmutable $start, DateTimeImmutable $end): array
    {
        $cols = [
            ...self::COLUMNS,
            'p.id as pId',
            'p.firstName',
            'p.lastName',
            'p.alias',
            'p.type',
            'p.roleId',
            'r.color',
            'r.icon',
            'r.name as roleName',
            'r.foreColor'
        ];
        $qb = $this->qb();
        $qb->select(...$cols)
            ->from(
                self::TABLE,
                self::ALIAS
            )
            ->leftJoin(
                self::ALIAS,
                ProfileRepository::TABLE,
                ProfileRepository::ALIAS,
                'p.id = v.profile'
            )->leftJoin(
                self::ALIAS,
                EventRepository::TABLE,
                EventRepository::ALIAS,
                'v.event = e.id'
            )
            ->leftJoin(
                'p',
                'role',
                'r',
                'r.id = p.roleId'
            )
            ->where(
                'v.id IS NOT NULL'
            )->andWhere('IFNULL(v.start, e.start) >= ' . $qb->createNamedParameter($start->format('Y-m-d H:i:s')))
            ->andWhere('IFNULL(v.end, e.end) < ' . $qb->createNamedParameter($end->format('Y-m-d H:i:s')))->addGroupBy(
                'v.id'
            );
        return $this->getResults($qb->executeQuery());
    }
}
