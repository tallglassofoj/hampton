<?php

declare(strict_types=1);

namespace App\Domain\Visit\Entity;

use App\Domain\Profile\Data\ProfileType;
use App\Domain\Profile\Entity\Profile;
use App\Domain\Role\Entity\Role;
use App\Domain\Role\Service\FetchRolesService;
use App\Service\LuminosityContrast;
use DateTimeImmutable;

class Visit
{
    public function __construct(
        private int $id,
        private int $event,
        private int|Profile $profile,
        private bool $approved,
        private ?DateTimeImmutable $start,
        private ?DateTimeImmutable $end,
        private ?DateTimeImmutable $edited,
    ) {
    }

    public static function new($data): self
    {
        if (isset($data->type)) {
            if ($data->type === ProfileType::INMATE->value) {
                $role = FetchRolesService::getInmateRole();
            } elseif ($data->type === ProfileType::GUARD->value) {
                $role = FetchRolesService::getGuardRole();
            } else {
                $role = new Role(
                    id: $data->roleId,
                    icon: $data->icon,
                    color: $data->color,
                    name: $data->roleName
                );
            }
            $profile = new Profile(
                roleId: $data->roleId,
                id: $data->pId,
                firstName: $data->firstName,
                lastName: $data->lastName,
                alias: $data->alias,
                type: ProfileType::tryFrom($data->type),
                role: $role,
                picture: null,
                visitName: 'Visit',
            );
            $data->profile = $profile;
        }
        return new self(
            id: $data->id,
            event: $data->event,
            profile: $data->profile,
            approved: (bool) $data->approved,
            start: $data->start ? new DateTimeImmutable($data->start) : null,
            end: $data->end ? new DateTimeImmutable($data->end) : null,
            edited: $data->edited ? new DateTimeImmutable($data->edited) : null,
        );
    }

    public function getEvent(): int
    {
        return $this->event;
    }

    public function getProfile(): int|Profile
    {
        return $this->profile;
    }

    public function isApproved(): bool
    {
        return $this->approved;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getStart(): ?DateTimeImmutable
    {
        return $this->start;
    }

    public function getEnd(): ?DateTimeImmutable
    {
        return $this->end;
    }

    public function getEdited(): ?DateTimeImmutable
    {
        return $this->edited;
    }

    public function getSimpleCalendarEntry(): array
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getProfile()->getName(),
            'backgroundColor' => $this->getProfile()->getRole()->getColor(),
            'textColor' => LuminosityContrast::getContrastColor($this->getProfile()->getRole()->getColor()),
            'start' => $this->getStart()->format(DateTimeImmutable::ATOM),
            'end' => $this->getEnd()->format(DateTimeImmutable::ATOM),
            'allDay' => false,
            'url' => '/visit/' . $this->getId(),
            'type' => $this->getProfile()->getRole()->getName()
        ];
    }
}
