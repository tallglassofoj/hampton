<?php

declare(strict_types=1);

namespace App\Domain\Visit\Service;

use App\Domain\Event\Data\EventTypeEnum;
use App\Domain\Event\Entity\Event;
use App\Domain\Event\Service\FetchEventService;
use App\Domain\Profile\Data\ProfileType;
use App\Domain\Profile\Entity\Profile;
use App\Domain\Role\Service\FetchRolesService;
use App\Domain\User\Entity\User;
use App\Domain\Visit\Repository\VisitRepository;
use App\Service\FlashMessageService;
use DateTimeImmutable;
use DI\Attribute\Inject;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CreateVisitService
{
    #[Inject()]
    private FetchEventService $eventService;

    #[Inject()]
    private FetchRolesService $rolesService;

    #[Inject()]
    private VisitRepository $visitRepository;

    #[Inject()]
    private FlashMessageService $flash;

    public function createNewVisit(
        int|Event $event,
        Profile $profile,
        User $user,
        ?DateTimeImmutable $start,
        ?DateTimeImmutable $end
    ) {
        if (is_int($event)) {
            $event = $this->eventService->getEventById($event);
        }
        //TODO: Validate:
        //  - Any deviations from the schedule (visit can not occur outside times set by the event itself)

        if ($start && $start < $event->getStart()) {
            throw new BadRequestException("Your visit can not start before the event");
        }

        if ($end && $end > $event->getEnd()) {
            throw new BadRequestException("Your visit can not end after the event");
        }

        //See if this event is open to whatever role the profile is tied to
        $roles = $this->rolesService->getRolesForEvent($event);
        foreach ($roles as $r) {
            //Match based on type first (Inmates & Guards)
            if ($profile->getType() === $r->getType()) {
                try {
                    $this->flash->addSuccessMessage("Your " . $profile->getVisitName() . " request has been created");
                    return $this->visitRepository->insertVisit(
                        $event,
                        $profile,
                        $user
                    );
                } catch (UniqueConstraintViolationException $e) {
                    throw new ConflictHttpException("You already have a " . $profile->getVisitName() . " for this event");
                }
            }
            //Match based on role ID (for custom roles)
            if ($profile->getRoleId() === $r->getId()) {
            }
        }
    }

    public function createNewSentence(
        Event $event,
        User $user,
        array $data
    ): int {
        $start = null;
        $end = null;
        if (!empty($data['start'])) {
            $start = new DateTimeImmutable($data['start']);
        }
        if (!empty($data['end'])) {
            $end = new DateTimeImmutable($data['end']);
        }
        return $this->createNewVisit(
            $event,
            $user->getInmate(),
            $user,
            $start,
            $end
        );
    }

    public function createNewShift(
        Event $event,
        User $user,
        array $data
    ): int {
        $start = null;
        $end = null;
        if (!empty($data['start'])) {
            $start = new DateTimeImmutable($data['start']);
        }
        if (!empty($data['end'])) {
            $end = new DateTimeImmutable($data['end']);
        }
        return $this->createNewVisit(
            $event,
            $user->getGuard(),
            $user,
            $start,
            $end
        );
    }
}
