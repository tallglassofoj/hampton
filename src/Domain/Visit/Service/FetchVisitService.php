<?php


declare(strict_types=1);

namespace App\Domain\Visit\Service;

use App\Domain\Event\Entity\Event;
use App\Domain\User\Entity\User;
use App\Domain\Visit\Entity\Visit;
use App\Domain\Visit\Repository\VisitRepository;
use DateTimeImmutable;

class FetchVisitService
{
    public function __construct(
        private VisitRepository $visitRepository
    ) {
    }

    public function getVisitById(int $id): ?Visit
    {
        $visit = $this->visitRepository->findOneBy('id', $id);
        return $visit;
    }

    public function getVisitsForUser(User $user): array
    {
        return $this->visitRepository->findVisitsForUser($user);
    }

    public function getVisitsForEvent(Event $event): array
    {
        return $this->visitRepository->findVisitsForEvent($event);
    }

    public function findVisitsBetween(DateTimeImmutable $start, DateTimeImmutable $end): array
    {
        return $this->visitRepository->findVisitsBetween($start, $end);
    }
}
