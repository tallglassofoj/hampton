<?php

declare(strict_types=1);

namespace App\Domain\Visit\Service;

use App\Domain\Event\Service\FetchEventService;
use App\Domain\User\Entity\User;
use App\Domain\Visit\Entity\Visit;
use App\Domain\Visit\Repository\VisitRepository;
use App\Service\FlashMessageService;
use DateTimeImmutable;
use DI\Attribute\Inject;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use DateTimeZone;

class UpdateVisitService
{
    #[Inject()]
    private FetchEventService $eventService;

    #[Inject()]
    private VisitRepository $visitRepository;

    #[Inject()]
    private FlashMessageService $flash;

    public function updateVisit(
        Visit $visit,
        User $user,
        string $start,
        string $end,
        string $timezone
    ) {

        $tz = new DateTimeZone($timezone);
        $start = (new DateTimeImmutable($start, $tz))->setTimezone(new DateTimeZone('UTC'));
        $end = (new DateTimeImmutable($end, $tz))->setTimezone(new DateTimeZone('UTC'));

        $event = $this->eventService->getEventById($visit->getEvent());
        //TODO: Validate:
        //  - Any deviations from the schedule (visit can not occur outside times set by the event itself)

        if ($start < $event->getStart()) {
            throw new BadRequestException("Your visit can not start before the event");
        }

        if ($end > $event->getEnd()) {
            throw new BadRequestException("Your visit can not end after the event");
        }
        $this->visitRepository->updateVisit(
            $visit,
            $user,
            $start,
            $end
        );
        $this->flash->addSuccessMessage("This visit has been updated");
    }

    public function approveVisit(
        Visit $visit,
        User $approver
    ) {
        $this->visitRepository->approveVisit(
            $visit,
            $approver
        );
        $this->flash->addSuccessMessage("This visit has been approved");
    }

    public function disapproveVisit(
        Visit $visit,
        User $approver
    ) {
        $this->visitRepository->disapproveVisit(
            $visit,
            $approver
        );
        $this->flash->addSuccessMessage("This visit approval has been revoked");
    }
}
