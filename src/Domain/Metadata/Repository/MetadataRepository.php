<?php

namespace App\Domain\Metadata\Repository;

use App\Repository\BaseQuery;
use App\Repository\Repository;
use Doctrine\DBAL\Query\QueryBuilder;

class MetadataRepository extends Repository implements BaseQuery
{
    public const TABLE = 'metadata';
    public const ALIAS = 'm';

    public const COLUMNS = [
        'm.id',
        'm.data_name as name',
        'm.data_value as value',
        'm.created',
        'm.updated'
    ];

    public function getBaseQuery(): QueryBuilder
    {
        $qb = $this->qb();
        $qb->from(self::TABLE, self::ALIAS);
        $qb->select(...self::COLUMNS);
        return $qb;
    }

}
