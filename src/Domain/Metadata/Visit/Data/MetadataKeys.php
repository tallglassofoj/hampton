<?php

declare(strict_types=1);

namespace App\Domain\Metadata\Visit\Data;

enum MetadataKeys: string
{
    case TRAVEL = 'travel';

    public function getMassageClass(): string
    {
        return match($this) {
            MetadataKeys::TRAVEL => TravelData::class
        };
    }
}
