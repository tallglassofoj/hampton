<?php

declare(strict_types=1);

namespace App\Domain\Metadata\Visit\Data;

class TravelData
{
    public const ARRIVAL_OPTIONS = [
        'flying' => 'Flying',
        'driving' => 'Driving'
    ];

    public const DEPARTURE_OPTIONS = [
        'flying' => 'Flying',
        'driving' => 'Driving'
    ];

    public const LASTLEG_OPTIONS = [
        'rental' => 'Renting a car',
        'carpool' => 'Carpooling'
    ];

    public static function massage($data)
    {
        if(!empty($data->lastLeg)) {
            $data->lastleg_h = self::LASTLEG_OPTIONS[$data->lastLeg];
        }
        return $data;
    }

}
