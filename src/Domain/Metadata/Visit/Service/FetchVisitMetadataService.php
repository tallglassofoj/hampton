<?php

declare(strict_types=1);

namespace App\Domain\Metadata\Visit\Service;

use App\Domain\Metadata\Visit\Repository\VisitMetadataRepository;
use App\Domain\Visit\Entity\Visit;

class FetchVisitMetadataService
{
    public function __construct(
        private VisitMetadataRepository $metadataRepository
    ) {

    }

    public function getMetadataForVisit(Visit $visit): array
    {
        $metadata = $this->metadataRepository->getMetadataForVisit($visit);
        $tmp = [];
        foreach($metadata as $m) {
            $tmp[$m->getName()->value] = $m;
        }
        return $tmp;
    }

}
