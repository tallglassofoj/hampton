<?php

declare(strict_types=1);

namespace App\Domain\Metadata\Visit\Service;

use App\Domain\Metadata\Visit\Data\MetadataKeys;
use App\Domain\Visit\Entity\Visit;

class UpdateVisitMetadataService
{
    public function __construct(
        private TravelMetadataService $travel
    ) {

    }

    public function updateMetadata(array $data, MetadataKeys $key, Visit $visit): int
    {
        switch ($key) {
            case MetadataKeys::TRAVEL:
                return $this->travel->updateMetadata($data, $visit);
                break;
        }
    }

}
