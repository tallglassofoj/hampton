<?php

declare(strict_types=1);

namespace App\Domain\Metadata\Visit\Service;

use App\Domain\Metadata\Visit\Data\MetadataKeys;
use App\Domain\Metadata\Visit\Repository\VisitMetadataRepository;
use App\Domain\Visit\Entity\Visit;
use App\Service\FlashMessageService;
use DI\Attribute\Inject;

class TravelMetadataService
{
    #[Inject()]
    private VisitMetadataRepository $metadataRepository;

    #[Inject()]
    private FlashMessageService $flash;

    public function updateMetadata(array $data, Visit $visit): int
    {
        $this->flash->addSuccessMessage("Your travel information has been updated");
        return $this->metadataRepository->updateOrInsertMetadata(
            $data,
            MetadataKeys::TRAVEL,
            $visit
        );
    }

}
