<?php

declare(strict_types=1);

namespace App\Domain\Metadata\Visit\Entity;

use App\Domain\Metadata\Visit\Data\MetadataKeys;
use DateTimeImmutable;

class VisitMetadata
{
    public function __construct(
        private int $id,
        private MetadataKeys $name,
        private object $value,
        private ?DateTimeImmutable $created,
        private ?DateTimeImmutable $updated
    ) {
        $this->massageData($this->value);
    }

    public static function new(object $data): static
    {
        return new self(
            id: $data->id,
            name: MetadataKeys::tryFrom($data->name),
            value: json_decode($data->value),
            created: $data->created ? new DateTimeImmutable($data->created) : null,
            updated: $data->updated ? new DateTimeImmutable($data->updated) : null,
        );
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getValue(): object
    {
        return $this->value;
    }

    public function getName(): MetadataKeys
    {
        return $this->name;
    }

    private function massageData(): static
    {
        $this->value = $this->getName()->getMassageClass()::massage($this->getValue());
        return $this;
    }
}
