<?php

declare(strict_types=1);

namespace App\Domain\Metadata\Visit\Repository;

use App\Domain\Metadata\Visit\Data\MetadataKeys;
use App\Domain\Metadata\Visit\Entity\VisitMetadata;
use App\Domain\Visit\Entity\Visit;
use App\Repository\BaseQuery;
use App\Repository\Repository;
use Doctrine\DBAL\Query\QueryBuilder;

class VisitMetadataRepository extends Repository implements BaseQuery
{
    public const TABLE = 'visit_metadata';
    public const ALIAS = 'm';

    public const COLUMNS = [
        'm.id',
        'm.data_name as name',
        'm.data_value as value',
        'm.created',
        'm.updated',
        'm.visit',
    ];

    public ?string $entityClass = VisitMetadata::class;

    public function getBaseQuery(): QueryBuilder
    {
        $qb = $this->qb();
        $qb->from(self::TABLE, self::ALIAS);
        $qb->select(...self::COLUMNS);
        return $qb;
    }

    public function updateOrInsertMetadata(array $data, MetadataKeys $key, Visit $visit): ?int
    {
        $v = $this->findKeyForVisit($key, $visit);
        $qb = $this->qb();
        if(!$v) {
            $qb->insert(self::TABLE)
            ->values([
                'data_name' => $qb->createNamedParameter($key->value),
                'data_value' => $qb->createNamedParameter(json_encode($data)),
                'visit' => $qb->createNamedParameter($visit->getId())
                ])->executeStatement();
            return (int) $this->connection->lastInsertId();
        } else {
            $qb->update(self::TABLE)
                ->set(
                    'data_value',
                    $qb->createNamedParameter(json_encode($data))
                )
                ->where(
                    "visit = ".$qb->createNamedParameter($visit->getId())
                )
                ->andWhere(
                    "data_name = ".$qb->createNamedParameter($key->value)
                )->executeStatement();
            return $v->getId();
        }

    }

    public function findOneBy(string $field, string|int $value): ?VisitMetadata
    {
        $qb = $this->getBaseQuery();
        $qb->where(self::ALIAS.".$field = ". $qb->createNamedParameter($value));
        return $this->getResult($qb->executeQuery());
    }

    public function findKeyForVisit(MetadataKeys $key, Visit $visit): ?VisitMetadata
    {
        $qb = $this->getBaseQuery();
        $qb->where(self::ALIAS.".data_name = ". $qb->createNamedParameter($key->value));
        $qb->andWhere(self::ALIAS.".visit = ".$qb->createNamedParameter($visit->getId()));
        return $this->getResult($qb->executeQuery());
    }

    public function getMetadataForVisit(Visit $visit): array
    {
        $qb = $this->getBaseQuery();
        $qb->where(self::ALIAS.".visit = ". $qb->createNamedParameter($visit->getId()));
        return $this->getResults($qb->executeQuery());
    }

}
