<?php

declare(strict_types=1);


namespace App\Domain\User\Data;

enum Permissions: int
{

    case REQUEST_BUILTIN = (1 << 1);
    case REQUEST_CUSTOM = (1 << 2);
    case SCHEDULE_EVENTS = (1 << 3);
    case VIEW_DETAILS = (1 << 4);
    case MANAGE_USERS = (1 << 5);
    case MANAGE_ROLES = (1 << 6);

    public function getName(): string
    {
        return match ($this) {
            default => '',
            Permissions::REQUEST_BUILTIN => "Request Sentences & Shifts",
            Permissions::REQUEST_CUSTOM => "Request Other Visits",
            Permissions::SCHEDULE_EVENTS => "Schedule Events",
            Permissions::VIEW_DETAILS => "View Calendar Details",
            Permissions::MANAGE_USERS => "Manage User Accounts",
            Permissions::MANAGE_ROLES => "Manage Profile Roles"
        };
    }
    public static function fromName(string $name): self
    {
        foreach (self::cases() as $status) {
            if ($name === $status->name) {
                return $status;
            }
        }
        throw new \ValueError("$name is not a valid backing value for enum " . self::class);
    }

    public static function tryFromName(string $name): self|null
    {
        try {
            return self::fromName($name);
        } catch (\ValueError $error) {
            return null;
        }
    }
}
