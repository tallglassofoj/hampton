<?php

declare(strict_types=1);

namespace App\Domain\User\Entity;

use App\Domain\Event\Data\EventTypeEnum;
use App\Domain\Profile\Data\ProfileType;
use App\Domain\Profile\Entity\Profile;
use App\Domain\Profile\Service\FetchProfileService;
use App\Domain\Role\Entity\Role;
use App\Domain\Role\Service\FetchRolesService;
use DateTimeImmutable;

class UserVisits
{
    public function __construct(
        private int $id, //Visit ID,
        private ?DateTimeImmutable $visitStart,
        private ?DateTimeImmutable $visitEnd,
        private bool $approved,
        private string $name,
        private DateTimeImmutable $eventStart,
        private DateTimeImmutable $eventEnd,
        private EventTypeEnum $type,
        private ?Profile $profile = null
    ) {
    }

    public static function new($data): static
    {
        if ($data->pType === ProfileType::INMATE->value) {
            $role = FetchRolesService::getInmateRole();
        } elseif ($data->pType === ProfileType::GUARD->value) {
            $role = FetchRolesService::getGuardRole();
        } else {
            $role = new Role(
                id: $data->roleId,
                icon: $data->icon,
                color: $data->color,
                name: $data->roleName
            );
        }
        $profile = new Profile(
            roleId: $data->roleId,
            id: $data->pId,
            firstName: $data->firstName,
            lastName: $data->lastName,
            alias: $data->alias,
            type: ProfileType::tryFrom($data->pType),
            role: $role,
            picture: null,
            visitName: 'Visit',
            userId: $data->userId
        );
        return new self(
            id: $data->id,
            visitStart: $data->vStart ? new DateTimeImmutable($data->vStart) : null,
            visitEnd: $data->vEnd ? new DateTimeImmutable($data->vEnd) : null,
            approved: (bool) $data->approved,
            name: $data->name,
            eventStart: $data->eStart ? new DateTimeImmutable($data->eStart) : null,
            eventEnd: $data->eEnd ? new DateTimeImmutable($data->eEnd) : null,
            type: EventTypeEnum::tryFrom($data->type),
            profile: $profile
        );
    }

    public function getStart(): DateTimeImmutable
    {
        if (!$this->getVisitStart()) {
            return $this->getEventStart();
        }
        return $this->getVisitStart();
    }

    public function getEnd(): DateTimeImmutable
    {
        if (!$this->getVisitEnd()) {
            return $this->getEventEnd();
        }
        return $this->getVisitEnd();
    }

    public function getVisitStart(): ?DateTimeImmutable
    {
        return $this->visitStart;
    }

    public function getVisitEnd(): ?DateTimeImmutable
    {
        return $this->visitEnd;
    }

    public function getEventStart(): DateTimeImmutable
    {
        return $this->eventStart;
    }

    public function getEventEnd(): DateTimeImmutable
    {
        return $this->eventEnd;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isApproved(): bool
    {
        return $this->approved;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }
}
