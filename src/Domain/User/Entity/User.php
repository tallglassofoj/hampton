<?php

declare(strict_types=1);

namespace App\Domain\User\Entity;

use App\Domain\Guard\Entity\Guard;
use App\Domain\Inmate\Entity\Inmate;
use App\Domain\Profile\Data\ProfileType;
use App\Domain\Profile\Entity\Profile;
use App\Domain\Rank\Entity\Rank;
use App\Domain\User\Data\Permissions;
use DateTime;
use DateTimeImmutable;
use DateTimeZone;

class User
{
    public function __construct(
        private int $id,
        private string $firstName,
        private string $lastName,
        private string $email,
        private string $password,
        private DateTimeImmutable $created,
        private int $createdIp,
        private Rank $rank,
        private bool $isConfirmed = false,
        private bool $isEnabled = false,
        private bool $isAdmin = false,
        private bool $resetPassword = false,
        private array $profiles = [],
        private ?Inmate $inmate = null,
        private ?Guard $guard = null,
    ) {
    }

    public static function new(object $data): self
    {
        if (0 == $data->rank) {
            $rank = Rank::getDefaultRank();
        } else {
            $rank = new Rank(
                id: $data->rank,
                name: $data->rankName,
                flags: $data->flags
            );
        }
        return new self(
            id: $data->id,
            firstName: $data->firstName,
            lastName: $data->lastName,
            email: $data->email,
            password: $data->password,
            created: new DateTimeImmutable($data->created, new DateTimeZone('UTC')),
            createdIp: $data->createdIp,
            rank: $rank,
            isEnabled: (bool) $data->isEnabled,
            resetPassword: (bool) $data->resetPassword
        );
    }

    public function checkPassword(string $providedPassword): bool
    {
        return password_verify($providedPassword, $this->getPassword());
    }

    public function getName(): string
    {
        return $this->getEmail();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setProfiles(array $profiles): static
    {
        $this->profiles = $profiles;
        foreach ($this->profiles as $p) {
            if ($p->getType() === ProfileType::INMATE) {
                $this->setInmateProfile($p);
            }
            if ($p->getType() === ProfileType::GUARD) {
                $this->setGuardProfile($p);
            }
        }
        return $this;
    }

    private function setInmateProfile(Inmate $inmate): static
    {
        $this->inmate = $inmate;
        return $this;
    }

    private function setGuardProfile(Guard $guard): static
    {
        $this->guard = $guard;
        return $this;
    }

    public function getInmate(): ?Inmate
    {
        return $this->inmate;
    }

    public function getGuard(): ?Guard
    {
        return $this->guard;
    }

    public function getRank(): Rank
    {
        return $this->rank;
    }

    public function has(Permissions|string $permission): bool
    {
        if ($this->requirePasswordReset()) {
            return false;
        }
        return $this->getRank()->has($permission);
    }

    public function isAdmin(): bool
    {
        return $this->isAdmin;
    }

    public function hasProfileForRole(?int $role): ?Profile
    {
        foreach ($this->getProfiles() as $p) {
            if ($p->getRoleId() === $role) {
                return $p;
            }
        }
        return null;
    }

    public function getProfileById(int $id): Profile
    {
        foreach ($this->getProfiles() as $p) {
            if ($p->getId() === $id) {
                return $p;
            }
        }
    }

    public function getProfiles(): array
    {
        return $this->profiles;
    }

    public function isConfirmed(): bool
    {
        return $this->isConfirmed;
    }

    public function isEnabled(): bool
    {
        return $this->isEnabled;
    }

    public function getCreated(): DateTimeImmutable
    {
        return $this->created;
    }

    public function requirePasswordReset(): bool
    {
        return $this->resetPassword;
    }
}
