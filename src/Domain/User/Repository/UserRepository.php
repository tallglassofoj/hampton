<?php

declare(strict_types=1);

namespace App\Domain\User\Repository;

use App\Domain\Rank\Entity\Rank;
use App\Domain\User\Entity\User;
use App\Repository\Repository;
use Doctrine\DBAL\ParameterType;
use Exception;

class UserRepository extends Repository
{
    public const TABLE = 'user';
    public const ALIAS = 'u';

    public const COLUMNS = [
        'u.id',
        'u.firstName',
        'u.lastName',
        'u.email',
        'u.password',
        'u.created',
        'u.created_ip as createdIp',
        'u.is_confirmed as isConfirmed',
        'u.is_enabled as isEnabled',
        'u.is_admin as isAdmin',
        'u.require_password_reset as resetPassword',
        'u.rank',
        'r.name as rankName',
        'r.flags'
    ];

    public ?string $entityClass = User::class;

    public function insertUser(
        string $firstName,
        string $lastName,
        string $email,
        string $password
    ): ?int {
        try {
            $qb = $this->qb();
            $qb->insert(self::TABLE)
                ->values([
                    'firstName' => $qb->createNamedParameter($firstName),
                    'lastName' => $qb->createNamedParameter($lastName),
                    'email' => $qb->createNamedParameter($email),
                    'password' => $qb->createNamedParameter($password),
                    'created_ip' => $qb->createNamedParameter(ip2long($_SERVER['REMOTE_ADDR']))
                ])->executeStatement();
            return (int) $this->connection->lastInsertId();
        } catch (Exception $e) {
            return null;
        }
    }

    public function findOneBy(string $field, string|int $value): ?User
    {
        $qb = $this->qb();
        $qb->select(...self::COLUMNS);
        $qb->from(self::TABLE, self::ALIAS);
        $qb->leftJoin(self::ALIAS, 'rank', 'r', 'r.id = u.rank');
        $qb->where(self::ALIAS . ".$field = " . $qb->createNamedParameter($value));
        return $this->getResult($qb->executeQuery());
    }

    public function findAllUsers(): array
    {
        $qb = $this->qb();
        $qb->select(...self::COLUMNS);
        $qb->from(self::TABLE, self::ALIAS);
        $qb->leftJoin(self::ALIAS, 'rank', 'r', 'r.id = u.rank');
        // $qb->where(self::ALIAS . ".$field = " . $qb->createNamedParameter($value));
        return $this->getResults($qb->executeQuery());
    }

    public function flipActivation(User $user): void
    {
        $qb = $this->qb();
        $qb->update(self::TABLE)
            ->set('is_enabled', $qb->createNamedParameter(!$user->isEnabled(), ParameterType::INTEGER))
            ->where('id = ' . $qb->createNamedParameter($user->getId()))
            ->executeStatement();
    }

    public function updateRank(User $user, Rank $rank): void
    {
        $qb = $this->qb();
        $qb->update(self::TABLE)
            ->set('rank', $qb->createNamedParameter($rank->getId(), ParameterType::INTEGER))
            ->where('id = ' . $qb->createNamedParameter($user->getId()))
            ->executeStatement();
    }

    public function updatePassword(User $user, string $password, bool $forceReset = true): void
    {
        $qb = $this->qb();
        $qb->update(self::TABLE)
            ->set('password', $qb->createNamedParameter($password))
            ->set('require_password_reset', $qb->createNamedParameter($forceReset, ParameterType::BOOLEAN))
            ->where('id = ' . $qb->createNamedParameter($user->getId()))
            ->executeStatement();
    }
}
