<?php

declare(strict_types=1);

namespace App\Domain\User\Service;

use App\Domain\User\Entity\User;
use App\Service\FlashMessageService;
use Exception;
use Symfony\Component\DependencyInjection\Attribute\Exclude;
use Symfony\Component\HttpFoundation\Session\Session;

class AuthenticateUserService
{
    public function __construct(
        private FetchUserService $fetchUser,
        private FlashMessageService $flash,
        private Session $session
    ) {
    }

    public function authenticateUserWithPassword(string $email, string $password): ?User
    {
        $user = $this->fetchUser->fetchUserByEmail($email);
        if (!$user) {
            throw new Exception("Email not found");
        }
        if (!$user->checkPassword($password)) {
            throw new Exception("Password is invalid");
        }
        $this->flash->addErrorMessage("You are now logged in");
        $this->session->set('user', $user->getId());
        return $user;
    }
}
