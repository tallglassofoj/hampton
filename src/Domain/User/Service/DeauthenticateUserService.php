<?php

declare(strict_types=1);

namespace App\Domain\User\Service;

use App\Service\FlashMessageService;
use Symfony\Component\HttpFoundation\Session\Session;

class DeauthenticateUserService
{
    public function __construct(
        private FlashMessageService $flash,
        private Session $session
    ) {
    }

    public function deauthenticateUser(string $message = "You have been logged out"): void
    {
        $this->session->invalidate();
        $this->flash->addSuccessMessage($message);
    }
}
