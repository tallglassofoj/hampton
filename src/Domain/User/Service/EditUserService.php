<?php

declare(strict_types=1);


namespace App\Domain\User\Service;

use App\Domain\Rank\Service\FindRankService;
use App\Domain\User\Data\Permissions;
use App\Domain\User\Entity\User;
use App\Domain\User\Repository\UserRepository;
use App\Service\FlashMessageService;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class EditUserService
{

    public function __construct(
        private UserRepository $userRepository,
        private FindRankService $rankService,
        private FlashMessageService $flash,
        private User $appUser
    ) {
    }

    public function toggleUser(User $user): bool
    {
        if ($user->getId() === $this->appUser->getId() && $this->appUser->isEnabled()) {
            $this->flash->addErrorMessage("You cannot deactivate your own account");
            return false;
        } else {
            $this->userRepository->flipActivation($user);
            return true;
        }
    }

    public function setRank(User $user, int $rank): void
    {
        $ranks = $this->rankService->listRanks();
        foreach ($ranks as $r) {
            if ($r->getId() === $rank) {
                $rank = $r;
            }
        }
        //Don't let a user downgrade themselves and be locked out of managing 
        //user accounts
        if ($user->getId() === $this->appUser->getId() && ($this->appUser->has(Permissions::MANAGE_USERS) && !$rank->has(Permissions::MANAGE_USERS))) {
            $this->flash->addErrorMessage("You cannot change your own rank to one that does not have permission to manage user accounts");
        } else {
            $this->userRepository->updateRank($user, $rank);
            $this->flash->addSuccessMessage("This users rank has been changed");
        }
    }

    public function setPassword(User $user, string $newPassword, string $verifyPassword, $forceReset = true): void
    {
        if ($newPassword !== $verifyPassword) {
            throw new BadRequestException("The passwords provided do not match", 400);
        }
        $newPassword = password_hash($newPassword, PASSWORD_DEFAULT);
        $this->userRepository->updatePassword($user, $newPassword, $forceReset);
        $this->flash->addSuccessMessage("This users password has been changed");
    }
}
