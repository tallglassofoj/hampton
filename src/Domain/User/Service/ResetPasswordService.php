<?php

declare(strict_types=1);

namespace App\Domain\User\Service;

use App\Domain\User\Entity\User;
use App\Service\FlashMessageService;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class ResetPasswordService
{

    public function __construct(
        private EditUserService $editService,
        private FlashMessageService $flash,
        private DeauthenticateUserService $deauthService
    ) {
    }

    public function setNewPassword(User $user, string $password, string $verifyPassword): void
    {
        if ($password !== $verifyPassword) {
            throw new BadRequestException("The passwords provided do not match", 400);
        }
        $this->editService->setPassword($user, $password, $verifyPassword, false);
        $this->deauthService->deauthenticateUser("Your password has been changed. Please log in again.");
    }
}
