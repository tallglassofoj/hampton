<?php

declare(strict_types=1);


namespace App\Domain\User\Service;

use App\Exception\ValidationException;
use Cake\Validation\Validator;

class ValidateUserService
{

    public static function validateUser(array $data): void
    {
        $validator = new Validator();
        $validator
            ->requirePresence('firstName')
            ->requirePresence('lastName')
            ->requirePresence('email')
            ->requirePresence('password')
            ->notEmptyString(
                'firstName',
                'A first name is required and cannot be blank'
            )->notEmptyString(
                'lastName',
                'A last name is required and cannot be blank'
            )->notEmptyString(
                'password',
                'A password is required and cannot be blank'
            )->email(
                'email',
                false,
                "A valid email address is required"
            );
        $errors = $validator->validate($data);
        if ($errors) {
            throw new ValidationException("Errors were detected", $errors);
        }
        // return true;
    }
}
