<?php

declare(strict_types=1);

namespace App\Domain\User\Service;

use App\Domain\User\Repository\UserRepository;
use App\Service\FlashMessageService;
use Exception;
use Symfony\Component\Mime\Address;

class CreateUserService
{
    public function __construct(
        private UserRepository $userRepository,
        private FetchUserService $fetchUser,
        private FlashMessageService $flash
    ) {
    }

    public function createNewUserFromRequest(
        string $firstName,
        string $lastName,
        string $email,
        string $password
    ): bool {
        ValidateUserService::validateUser([$firstName, $lastName, $email, $password]);
        $email = new Address($email, "$firstName, $lastName");
        if ($this->fetchUser->fetchUserByEmail($email->getAddress())) {
            //Email address is already in use
            throw new Exception("Your account could not be created at this time", 500);
        }
        //TODO: Validate names etc
        if ($this->userRepository->insertUser(
            firstName: $firstName,
            lastName: $lastName,
            email: $email->getAddress(),
            password: password_hash($password, PASSWORD_DEFAULT)
        )) {
            $this->flash->addSuccessMessage("Your user account has been created");
            return true;
        } else {
            $this->flash->addErrorMessage("Your user account could not be created");
            return true;
        }
    }
}
