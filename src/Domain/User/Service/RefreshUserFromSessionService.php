<?php

namespace App\Domain\User\Service;

use App\Domain\Profile\Repository\ProfileRepository;
use App\Domain\User\Entity\User;
use App\Domain\User\Repository\UserRepository;
use Doctrine\DBAL\Connection;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class RefreshUserFromSessionService
{
    private UserRepository $userRepository;
    private ProfileRepository $profileRepository;

    public function __construct(private ContainerInterface $container)
    {
        $this->userRepository = new UserRepository($container->get(Connection::class));
        $this->profileRepository = new ProfileRepository($container->get(Connection::class));
    }

    public function refreshUser(): ?User
    {
        $session = $this->container->get(Session::class);
        $user = $session->get('user', null);
        if ($user) {
            $user = $this->userRepository->findOneBy('id', (int) $session->get('user'));
            $profiles = $this->profileRepository->getProfilesByUser($user);
            $user->setProfiles($profiles);
            return $user;
        }
        return null;
    }
}
