<?php

declare(strict_types=1);

namespace App\Domain\User\Service;

use App\Domain\User\Entity\User;
use App\Domain\User\Repository\UserRepository;
use Exception;

class FetchUserService
{
    public function __construct(
        private UserRepository $userRepository
    ) {
    }

    public function fetchUserByEmail(string $email): ?User
    {
        return $this->userRepository->findOneBy('email', $email);
    }

    public function fetchUserById(int $id): ?User
    {
        if (DEBUG) {
            return $this->userRepository->findOneBy('id', $id);
        }
        try {
            return $this->userRepository->findOneBy('id', $id);
        } catch (Exception $e) {
            return null;
        }
    }

    public function fetchAllUsers(): array
    {
        return $this->userRepository->findAllUsers();
    }
}
