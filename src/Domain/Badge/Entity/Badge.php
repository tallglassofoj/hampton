<?php

declare(strict_types=1);

namespace App\Domain\Badge\Entity;

use App\Domain\Profile\Entity\Profile;

class Badge
{
    public function __construct(
        private string $name,
        private string $bgColor,
        private string $foreColor,
        private string $icon,
        private string $role,
        private ?int $user = null
    ) {
    }
    public static function newFromProfile(
        Profile $profile
    ): static {
        return new self(
            name: $profile->getName(),
            bgColor: $profile->getRole()->getColor(),
            foreColor: $profile->getRole()->getForeColor(),
            icon: $profile->getRole()->getIcon(),
            role: $profile->getRole()->getName(),
            user: $profile->getUserId()
        );
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getBgColor(): string
    {
        return $this->bgColor;
    }

    public function getForeColor(): string
    {
        return $this->foreColor;
    }

    public function getIcon(): string
    {
        return $this->icon;
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function getUser(): ?int
    {
        return $this->user;
    }
}
