<?php

declare(strict_types=1);


namespace App\Domain\Rank\Repository;

use App\Domain\Rank\Entity\Rank;
use App\Repository\Repository;

class RankRepository extends Repository
{

    public ?string $entityClass = Rank::class;

    public const COLUMNS = [
        'r.id',
        'r.name',
        'r.flags'
    ];

    public const TABLE = 'rank';
    public const ALIAS = 'r';

    public function insertNewRank(
        string $name,
        int $flags
    ): int {
        $qb = $this->qb();
        $qb->insert(self::TABLE)
            ->values([
                'name' => $qb->createNamedParameter($name),
                'flags' => $qb->createNamedParameter($flags)
            ])->executeStatement();
        return (int) $this->connection->lastInsertId();
    }

    public function updateRank(
        string $name,
        int $flags,
        Rank $rank,
    ): int {
        $qb = $this->qb();
        $qb->update(self::TABLE)
            ->set('flags', $qb->createNamedParameter($flags))
            ->set('name', $qb->createNamedParameter($name))
            ->where('id = ' . $qb->createNamedParameter($rank->getId()))
            ->executeStatement();
        return $rank->getId();
    }

    public function getRanks(): array
    {
        $qb = $this->qb();
        $result = $qb->select(...self::COLUMNS)
            ->from(self::TABLE, self::ALIAS)
            ->executeQuery();
        return $this->getResults($result);
    }
    public function getRank(int $id): Rank
    {
        $qb = $this->qb();
        $result = $qb->select(...self::COLUMNS)
            ->from(self::TABLE, self::ALIAS)
            ->where(self::ALIAS . ".id =" . $qb->createNamedParameter($id))
            ->executeQuery();
        return $this->getResult($result);
    }
}
