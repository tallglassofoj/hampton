<?php

declare(strict_types=1);

namespace App\Domain\Rank\Service;

use App\Exception\ValidationException;
use Cake\Validation\Validator;

class ValidateRankService
{
    public static function validateUser(array $data): void
    {
        $validator = new Validator();
        $validator
            ->requirePresence('name')
            ->requirePresence('flags')
            ->notEmptyString(
                'name',
                'You must specify a name for this rank'
            )->notEmptyArray(
                'flags',
                'You must set at least one flag'
            );
        $errors = $validator->validate($data);
        if ($errors) {
            throw new ValidationException("Errors were detected", $errors);
        }
        // return true;
    }
}
