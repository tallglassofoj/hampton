<?php

declare(strict_types=1);

namespace App\Domain\Rank\Service;

use App\Domain\Rank\Entity\Rank;
use App\Domain\Rank\Repository\RankRepository;
use App\Service\FlashMessageService;

class EditRankService
{
    public function __construct(
        private RankRepository $rankRepository,
        private FindRankService $rankService,
        private FlashMessageService $flash
    ) {
    }

    public function editRank(
        string $name,
        int $flags,
        Rank|int $rank
    ): int {
        if (!$rank instanceof Rank) {
            $rank = $this->rankService->getRankById($rank);
        }
        $this->rankRepository->updateRank($name, $flags, $rank);
        $this->flash->addSuccessMessage("This rank has been updated");
        return $rank->getId();
    }
}
