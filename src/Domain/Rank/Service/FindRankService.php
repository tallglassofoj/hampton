<?php

declare(strict_types=1);


namespace App\Domain\Rank\Service;

use App\Domain\Rank\Entity\Rank;
use App\Domain\Rank\Repository\RankRepository;

class FindRankService
{

    public function __construct(
        private RankRepository $rankRepository
    ) {
    }

    public function listRanks(): array
    {

        $ranks = [];
        $ranks[] = Rank::getDefaultRank();
        $ranks = [...$ranks, ...$this->rankRepository->getRanks()];
        return $ranks;
    }

    public function getRankById(int $id): Rank
    {
        return $this->rankRepository->getRank($id);
    }
}
