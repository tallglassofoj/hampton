<?php

declare(strict_types=1);

namespace App\Domain\Rank\Service;

use App\Domain\Rank\Repository\RankRepository;

class CreateRankService
{
    public function __construct(
        private RankRepository $rankRepository
    ) {
    }

    public function createNewRank(
        string $name,
        int $flags
    ): int {
        return $this->rankRepository->insertNewRank($name, $flags);
    }
}
