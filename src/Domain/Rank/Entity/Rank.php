<?php

declare(strict_types=1);


namespace App\Domain\Rank\Entity;

use App\Domain\User\Data\Permissions;

class Rank
{

    private array $permissions = [];

    public function __construct(
        private int $id,
        private string $name,
        private int $flags
    ) {
        foreach (Permissions::cases() as $p) {
            if ($p->value & $this->flags) {
                $this->permissions[$p->name] = true;
            }
        }
    }

    public static function getDefaultRank(): self
    {
        $flags = array_sum([
            Permissions::REQUEST_BUILTIN->value,
            Permissions::REQUEST_CUSTOM->value
        ]);
        return new self(0, 'User', $flags);
    }

    public static function new(object $data): self
    {
        return new self(
            id: $data->id,
            name: $data->name,
            flags: $data->flags
        );
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getFlags(): int
    {
        return $this->flags;
    }

    public function getPermissions(): array
    {
        return $this->permissions;
    }
    public function has(Permissions|string $permission): bool
    {
        if (!$permission instanceof Permissions) {
            $permission = Permissions::fromName($permission);
        }
        if (in_array(
            $permission->name,
            array_keys($this->getPermissions())
        )) {
            return true;
        }
        return false;
    }
}
