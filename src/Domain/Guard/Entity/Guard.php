<?php

declare(strict_types=1);

namespace App\Domain\Guard\Entity;

use App\Domain\Profile\Data\ProfileType;
use App\Domain\Profile\Entity\Profile;
use App\Domain\Role\Service\FetchRolesService;

class Guard extends Profile
{
    public const GUARD_COLOR = '#2c702c';
    public const GUARD_ICON = 'fa-solid fa-person-military-pointing';
    public const VISIT_NAME = 'Shift';
    public const VISIT_ROUTE = 'shift.request';

    public function __construct(
        $id,
        $firstName,
        $lastName,
        $alias,
        $picture
    ) {
        parent::__construct(
            roleId: null,
            id: $id,
            firstName: $firstName,
            lastName: $lastName,
            alias: $alias,
            picture: $picture,
            role: FetchRolesService::getGuardRole(),
            type: ProfileType::GUARD,
            visitName: self::VISIT_NAME
        );
    }

    public static function new(object $data)
    {
        return new self(
            id: $data->id,
            firstName: $data->firstName,
            lastName: $data->lastName,
            alias: $data->alias,
            picture: $data->picture
        );
    }

}
