<?php

declare(strict_types=1);

namespace App\Domain\Guard\Service;

use App\Domain\Profile\Data\ProfileType;
use App\Domain\Profile\Repository\ProfileRepository;
use App\Domain\User\Entity\User;
use App\Service\FlashMessageService;

class CreateGuardProfileService
{
    public function __construct(
        private ProfileRepository $profileRepository,
        private FlashMessageService $flash
    ) {

    }

    public function createGuard(
        ?string $firstName,
        ?string $lastName,
        string $alias,
        User $user
    ) {
        //Validate data
        $this->flash->addSuccessMessage("Your guard profile has been created");
        return $this->profileRepository->insertProfile(
            firstName: $firstName,
            lastName: $lastName,
            alias: $alias,
            type: ProfileType::GUARD,
            roleId: null,
            user: $user
        );
    }

}
