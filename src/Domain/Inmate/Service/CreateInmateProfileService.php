<?php

declare(strict_types=1);

namespace App\Domain\Inmate\Service;

use App\Domain\Profile\Data\ProfileType;
use App\Domain\Profile\Repository\ProfileRepository;
use App\Domain\User\Entity\User;
use App\Service\FlashMessageService;

readonly class CreateInmateProfileService
{
    public function __construct(
        private ProfileRepository   $profileRepository,
        private FlashMessageService $flash
    ) {

    }

    public function createInmate(
        ?string $firstName,
        ?string $lastName,
        string $alias,
        User $user
    ): int
    {
        //Validate data
        $this->flash->addSuccessMessage("Your inmate profile has been created");
        return $this->profileRepository->insertProfile(
            firstName: $firstName,
            lastName: $lastName,
            alias: $alias,
            type: ProfileType::INMATE,
            roleId: null,
            user: $user
        );
    }

}
