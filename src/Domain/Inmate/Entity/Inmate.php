<?php

declare(strict_types=1);

namespace App\Domain\Inmate\Entity;

use App\Domain\Profile\Data\ProfileType;
use App\Domain\Profile\Entity\Profile;
use App\Domain\Role\Service\FetchRolesService;

class Inmate extends Profile
{
    public const INMATE_COLOR = '#ffa500';
    public const INMATE_ICON = 'fa-solid fa-handcuffs';
    public const VISIT_NAME = 'Sentence';
    public const VISIT_ROUTE = 'sentence.request';

    public function __construct(
        int $id,
        ?string $firstName,
        ?string $lastName,
        string $alias,
        ?string $picture
    ) {
        parent::__construct(
            roleId: null,
            id: $id,
            firstName: $firstName,
            lastName: $lastName,
            alias: $alias,
            picture: $picture,
            type: ProfileType::INMATE,
            visitName: self::VISIT_NAME,
            role: FetchRolesService::getInmateRole()
        );
    }

    public static function new(object $data)
    {
        return new self(
            id: $data->id,
            firstName: $data->firstName,
            lastName: $data->lastName,
            alias: $data->alias,
            picture: $data->picture
        );
    }

}
