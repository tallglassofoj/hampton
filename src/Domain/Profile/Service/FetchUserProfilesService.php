<?php

declare(strict_types=1);

namespace App\Domain\Profile\Service;

use App\Domain\Profile\Repository\ProfileRepository;
use App\Domain\User\Entity\User;

class FetchUserProfilesService
{
    public function __construct(
        private User $user,
        private ProfileRepository $profileRepository
    ) {
    }

    public function getProfilesForUser(User $user): array
    {
        return $this->profileRepository->getProfilesByUser($user);
    }
}
