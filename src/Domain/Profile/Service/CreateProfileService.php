<?php

declare(strict_types=1);

namespace App\Domain\Profile\Service;

use App\Domain\Profile\Data\ProfileType;
use App\Domain\Profile\Repository\ProfileRepository;
use App\Domain\Role\Service\FetchRolesService;
use App\Domain\User\Entity\User;
use App\Service\FlashMessageService;

readonly class CreateProfileService
{
    public function __construct(
        private ProfileRepository   $profileRepository,
        private FlashMessageService $flash,
        private FetchRolesService $roleService
    ) {
    }

    public function createProfile(
        ?string $firstName,
        ?string $lastName,
        string $alias,
        User $user,
        int $role
    ): int {
        //Validate data
        $role = $this->roleService->getRoleById($role);
        return $this->profileRepository->insertProfile(
            firstName: $firstName,
            lastName: $lastName,
            alias: $alias,
            type: ProfileType::CUSTOM,
            role: $role,
            user: $user
        );
    }
}
