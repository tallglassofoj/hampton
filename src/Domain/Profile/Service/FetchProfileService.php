<?php

declare(strict_types=1);

namespace App\Domain\Profile\Service;

use App\Domain\Profile\Repository\ProfileRepository;
use App\Domain\User\Entity\User;

class FetchProfileService
{
    public function __construct(
        private ProfileRepository $profileRepository
    ) {

    }

    public function getProfileById(int $id)
    {
        return $this->profileRepository->findOneBy('id', $id);
    }

}
