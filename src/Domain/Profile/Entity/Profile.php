<?php

declare(strict_types=1);

namespace App\Domain\Profile\Entity;

use App\Domain\Badge\Entity\Badge;
use App\Domain\Profile\Data\ProfileType;
use App\Domain\Role\Entity\Role;

class Profile
{
    private ?Badge $badge = null;

    public function __construct(
        private ?int $roleId,
        private int $id,
        private ?string $firstName,
        private ?string $lastName,
        private string $alias,
        private ?string $picture,
        private ?Role $role,
        private ProfileType $type = ProfileType::CUSTOM,
        private string $visitName = 'Visit',
        private ?int $userId = null
    ) {
        if ($this->role) {
            $this->generateBadge();
        }
    }

    public static function new(object $data)
    {
        $type = ProfileType::tryFrom($data->type);
        $entity = $type->entityClass();
        if ($type != ProfileType::CUSTOM) {
            return $entity::new($data);
        } else {
            $role = new Role(
                id: $data->rId,
                icon: $data->rIcon,
                color: $data->rColor,
                name: $data->rName
            );
            return new self(
                roleId: $data->rId,
                id: $data->id,
                firstName: $data->firstName,
                lastName: $data->lastName,
                alias: $data->alias,
                role: $role,
                picture: $data->picture,
                userId: $data->userId
            );
        }
    }

    public function getType(): ProfileType
    {
        return $this->type;
    }

    public function getName(): string
    {
        return sprintf("%s %s %s", $this->getFirstName(), "'" . $this->getAlias() . "'", $this->getLastName());
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function getAlias(): string
    {
        return $this->alias;
    }

    public function getRoleId(): ?int
    {
        return $this->roleId;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getVisitName(): string
    {
        return $this->visitName;
    }

    public function getRole(): ?Role
    {
        return $this->role;
    }

    public function getBadge(): ?Badge
    {
        return $this->badge;
    }

    private function generateBadge(): static
    {
        $this->badge = Badge::newFromProfile($this);
        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }
}
