<?php

declare(strict_types=1);

namespace App\Domain\Profile\Data;

use App\Domain\Guard\Entity\Guard;
use App\Domain\Inmate\Entity\Inmate;
use App\Domain\Profile\Entity\Profile;

enum ProfileType: string
{
    case CUSTOM = 'Custom';
    case INMATE = 'Inmate';
    case GUARD = 'Guard';


    public function entityClass(): string
    {
        return match($this) {
            ProfileType::GUARD => Guard::class,
            ProfileType::INMATE => Inmate::class,
            ProfileType::CUSTOM => Profile::class
        };
    }
}
