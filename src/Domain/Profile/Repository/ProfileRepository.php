<?php

declare(strict_types=1);

namespace App\Domain\Profile\Repository;

use App\Domain\Profile\Data\ProfileType;
use App\Domain\Profile\Entity\Profile;
use App\Domain\Role\Entity\Role;
use App\Domain\User\Entity\User;
use App\Repository\BaseQuery;
use App\Repository\Repository;
use Doctrine\DBAL\Query\QueryBuilder;

class ProfileRepository extends Repository implements BaseQuery
{
    public const TABLE = 'profile';
    public const ALIAS = 'p';

    public ?string $entityClass = Profile::class;

    public const COLUMNS = [
        'p.id',
        'p.firstName',
        'p.lastName',
        'p.alias',
        'p.picture',
        'p.type',
        'p.user as userId',
        'r.id as rId',
        'r.name as rName',
        'r.color as rColor',
        'r.icon as rIcon',
    ];

    public function getBaseQuery(): QueryBuilder
    {
        $qb = $this->qb();
        $qb->from(self::TABLE, self::ALIAS);
        $qb->select(...self::COLUMNS);
        $qb->leftJoin(self::ALIAS, 'role', 'r', 'r.id = p.roleId');
        $qb->groupBy(self::ALIAS . '.id');
        return $qb;
    }

    public function getProfilesByUser(User $user): array
    {
        $qb = $this->getBaseQuery();
        $result = $qb->where('p.user = ' . $qb->createNamedParameter($user->getId()))->executeQuery();
        return $this->getResults($result);
    }

    public function findOneBy(string $field, string|int $value): ?Profile
    {
        $qb = $this->getBaseQuery();
        $qb->where(self::ALIAS . ".$field = " . $qb->createNamedParameter($value));
        return $this->getResult($qb->executeQuery());
    }

    public function insertProfile(
        string $firstName,
        string $lastName,
        string $alias,
        ProfileType $type,
        ?Role $role,
        User $user
    ): int {
        $qb = $this->qb();
        $qb->insert(self::TABLE)
            ->values([
                'firstName' => $qb->createNamedParameter($firstName),
                'lastName' => $qb->createNamedParameter($lastName),
                'alias' => $qb->createNamedParameter($alias),
                'type' => $qb->createNamedParameter($type->value),
                'roleId' => $qb->createNamedParameter($role?->getId()),
                'user' => $qb->createNamedParameter($user->getId())
            ])->executeStatement();
        return (int) $this->connection->lastInsertId();
    }
}
