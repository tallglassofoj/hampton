<?php

declare(strict_types=1);

namespace App\Domain\Event\Repository;

use App\Domain\Event\Data\EventTypeEnum;
use App\Domain\Event\Entity\Event;
use App\Domain\User\Entity\User;
use App\Repository\BaseQuery;
use App\Repository\Repository;
use DateTimeImmutable;
use Doctrine\DBAL\Query\QueryBuilder;

class EventRepository extends Repository implements BaseQuery
{
    public ?string $entityClass = Event::class;

    public const TABLE = 'event';
    public const ALIAS = 'e';

    public const COLUMNS = [
        'e.id',
        'e.start',
        'e.end',
        'e.type',
        'e.name',
        'e.description',
        'e.creator',
        'e.created',
        'e.edited',
        'u.email'
    ];

    public function getBaseQuery(): QueryBuilder
    {
        $qb = $this->qb();
        $qb->from(self::TABLE, self::ALIAS);
        $qb->select(...self::COLUMNS);
        $qb->leftJoin(self::ALIAS, 'user', 'u', 'e.creator = u.id');
        return $qb;
    }

    public function insertEvent(
        DateTimeImmutable $start,
        DateTimeImmutable $end,
        User $creator,
        string $name,
        string $desc,
        EventTypeEnum $type
    ): int {
        $qb = $this->qb();
        $qb->insert(self::TABLE)
            ->values([
                'start' => $qb->createNamedParameter($start->format('Y-m-d H:i:s')),
                'end' => $qb->createNamedParameter($end->format('Y-m-d H:i:s')),
                'creator' => $qb->createNamedParameter($creator->getId()),
                'name' => $qb->createNamedParameter($name),
                'description' => $qb->createNamedParameter($desc),
                'type' => $qb->createNamedParameter($type->value)
            ])->executeStatement();
        return (int) $this->connection->lastInsertId();
    }

    public function updateEvent(
        Event $event,
        DateTimeImmutable $start,
        DateTimeImmutable $end,
        User $editor,
        string $name,
        string $desc,
    ): int {
        $qb = $this->qb();
        $qb->update(self::TABLE)
            ->set(
                'start',
                $qb->createNamedParameter($start->format('Y-m-d H:i:s'))
            )
            ->set(
                'end',
                $qb->createNamedParameter($end->format('Y-m-d H:i:s'))
            )
            ->set(
                'name',
                $qb->createNamedParameter($name)
            )
            ->set(
                'description',
                $qb->createNamedParameter($desc)
            )
            ->set(
                'editor',
                $qb->createNamedParameter($editor->getId())
            )
            ->where('id = ' . $qb->createNamedParameter($event->getId()))
            ->executeStatement();
        return $event->getId();
    }

    public function findOneBy(string $field, string|int $value): ?Event
    {
        $qb = $this->getBaseQuery();
        $qb->where(self::ALIAS . ".$field = " . $qb->createNamedParameter($value));
        return $this->getResult($qb->executeQuery());
    }

    public function getEvents(): array
    {
        $qb = $this->getBaseQuery();
        return $this->getResults($qb->executeQuery());
    }

    public function insertEventRoles(int $event, array $roles): void
    {
        //Clear any previous roles, just in case
        $qb = $this->qb();
        $qb->delete("event_roles")
            ->where("event = " . $qb->createNamedParameter($event))
            ->executeStatement();

        foreach ($roles as $r) {
            $qb->insert("event_roles")
                ->values([
                    'event' => $qb->createNamedParameter($event),
                    'role' => $qb->createNamedParameter($r['id']),
                    'role_type' => $qb->createNamedParameter($r['type'])
                ])->executeStatement();
        }
    }

    public function findEventsBetween(DateTimeImmutable $start, DateTimeImmutable $end): array
    {
        $qb = $this->getBaseQuery();
        $result = $qb->where(self::ALIAS . '.start >= ' . $qb->createNamedParameter($start->format('Y-m-d H:i:s')), self::ALIAS . '.end < ' . $qb->createNamedParameter($end->format('Y-m-d H:i:s')))->executeQuery();
        return $this->getResults($result);
    }
}
