<?php

declare(strict_types=1);

namespace App\Domain\Event\Service;

use App\Domain\Event\Data\EventTypeEnum;
use App\Domain\Event\Repository\EventRepository;
use App\Domain\Profile\Data\ProfileType;
use App\Domain\User\Entity\User;
use App\Service\FlashMessageService;
use DateTimeImmutable;
use DateTimeZone;

class CreateEventService
{
    public function __construct(
        private EventRepository $eventRepository,
        private FlashMessageService $flash
    ) {
    }

    public function createNewEvent(
        string $start,
        string $end,
        string $name,
        string $desc,
        string $type,
        array $roles,
        User $creator,
        string $timezone
    ) {
        foreach ($roles as &$r) {
            if (ProfileType::GUARD->value === $r || ProfileType::INMATE->value === $r) {
                $id = null;
                $roleType = $r;
            } else {
                $id = $r;
                $roleType = ProfileType::CUSTOM->value;
            }
            $r = [
                'id' => $id,
                'type' => $roleType,
            ];
        }
        $tz = new DateTimeZone($timezone);
        $start = (new DateTimeImmutable($start, $tz))->setTimezone(new DateTimeZone('UTC'));
        $end = (new DateTimeImmutable($end, $tz))->setTimezone(new DateTimeZone('UTC'));
        //TODO: Validate start & end dates
        $type = EventTypeEnum::tryFrom($type);
        //TODO: Validate name & desc
        $this->flash->addSuccessMessage("Your event has been created");
        $id = $this->eventRepository->insertEvent(
            $start,
            $end,
            $creator,
            $name,
            $desc,
            $type
        );
        $this->eventRepository->insertEventRoles($id, $roles);
        return $id;
    }
}
