<?php

declare(strict_types=1);

namespace App\Domain\Event\Service;

use App\Domain\Event\Entity\Event;
use App\Domain\Event\Repository\EventRepository;
use App\Domain\Role\Service\FetchRolesService;
use DateTimeImmutable;

class FetchEventService
{
    public function __construct(
        private EventRepository $eventRepository,
        private FetchRolesService $roleService
    ) {
    }

    public function getUpcomingEvents(): array
    {
        return $this->eventRepository->getEvents();
    }

    public function getEventById(int $id, bool $full = false): Event
    {
        $event = $this->eventRepository->findOneBy('id', $id);
        if ($full) {
            $roles = $this->roleService->getRolesForEvent($event);
            $event->setAllowedRoles($roles);
        }
        return $event;
    }

    public function findEventsBetween(DateTimeImmutable $start, DateTimeImmutable $end): array
    {
        return $this->eventRepository->findEventsBetween($start, $end);
    }
}
