<?php

declare(strict_types=1);

namespace App\Domain\Event\Service;

use App\Domain\Event\Data\EventTypeEnum;
use App\Domain\Event\Entity\Event;
use App\Domain\Event\Repository\EventRepository;
use App\Domain\Profile\Data\ProfileType;
use App\Domain\User\Entity\User;
use App\Service\FlashMessageService;
use DateTimeImmutable;
use DateTimeZone;

class EditEventService
{
    public function __construct(
        private EventRepository $eventRepository,
        private FlashMessageService $flash
    ) {
    }

    public function updateEvent(
        Event $event,
        string $start,
        string $end,
        string $name,
        string $desc,
        array $roles,
        User $editor,
        string $timezone
    ) {
        foreach ($roles as &$r) {
            if (ProfileType::GUARD->value === $r || ProfileType::INMATE->value === $r) {
                $id = null;
                $roleType = $r;
            } else {
                $id = $r;
                $roleType = ProfileType::CUSTOM->value;
            }
            $r = [
                'id' => $id,
                'type' => $roleType,
            ];
        }
        $tz = new DateTimeZone($timezone);
        $start = (new DateTimeImmutable($start));
        $end = (new DateTimeImmutable($end));
        //TODO: Validate start & end dates
        //TODO: Validate name & desc
        $this->flash->addSuccessMessage("This event has been updated");
        $id = $this->eventRepository->updateEvent(
            $event,
            $start,
            $end,
            $editor,
            $name,
            $desc,
        );
        $this->eventRepository->insertEventRoles($id, $roles);
        return $id;
    }
}
