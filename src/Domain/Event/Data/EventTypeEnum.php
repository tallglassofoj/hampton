<?php

declare(strict_types=1);

namespace App\Domain\Event\Data;

enum EventTypeEnum: string
{
    case RP = 'Roleplay';
    case FILMING = 'Filming';
    case PRIVATE = 'Private';


    public function desc(): string
    {
        return match ($this) {
            EventTypeEnum::RP => "A standard prison roleplay event",
            EventTypeEnum::FILMING => "Filming a movie",
            EventTypeEnum::PRIVATE => "A private event"
        };
    }

    public function getCssClass(): string
    {
        return match ($this) {
            EventTypeEnum::RP => "info",
            EventTypeEnum::FILMING => "danger",
            EventTypeEnum::PRIVATE => "dark"
        };
    }

    public function isPrivate(): bool
    {
        return match ($this) {
            default => false,
            EventTypeEnum::PRIVATE => true
        };
    }

    public function getForeColor(): string
    {
        return match ($this) {
            EventTypeEnum::RP => "#0DCAF0",
            EventTypeEnum::FILMING => "#DC3545",
            EventTypeEnum::PRIVATE => "#222222"
        };
    }
}
