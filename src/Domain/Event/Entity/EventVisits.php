<?php

declare(strict_types=1);

namespace App\Domain\Event\Entity;

use App\Domain\Profile\Data\ProfileType;
use App\Domain\Profile\Entity\Profile;
use App\Domain\Role\Entity\Role;
use App\Domain\Role\Service\FetchRolesService;
use DateTimeImmutable;

class EventVisits
{
    public function __construct(
        private int $id,
        private Profile $profile,
        private ?DateTimeImmutable $start,
        private ?DateTimeImmutable $end,
        private bool $approved = false,
    ) {
    }

    public static function new(object $data): static
    {
        if ($data->type === ProfileType::INMATE->value) {
            $role = FetchRolesService::getInmateRole();
        } elseif ($data->type === ProfileType::GUARD->value) {
            $role = FetchRolesService::getGuardRole();
        } else {
            $role = new Role(
                id: $data->roleId,
                icon: $data->icon,
                color: $data->color,
                name: $data->roleName
            );
        }
        $profile = new Profile(
            roleId: $data->roleId,
            id: $data->pId,
            firstName: $data->firstName,
            lastName: $data->lastName,
            alias: $data->alias,
            type: ProfileType::tryFrom($data->type),
            role: $role,
            picture: null,
            visitName: 'Visit',
        );
        return new self(
            id: $data->id,
            profile: $profile,
            start: $data->vStart ? new DateTimeImmutable($data->vStart) : null,
            end: $data->vEnd ? new DateTimeImmutable($data->vEnd) : null,
            approved: (bool) $data->approved
        );
    }


    public function getProfile(): Profile
    {
        return $this->profile;
    }

    public function getStart(): ?DateTimeImmutable
    {
        return $this->start;
    }

    public function getEnd(): ?DateTimeImmutable
    {
        return $this->end;
    }

    public function isApproved(): bool
    {
        return $this->approved;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
