<?php

declare(strict_types=1);

namespace App\Domain\Event\Entity;

use App\Domain\Event\Data\EventTypeEnum;
use App\Domain\Profile\Data\ProfileType;
use App\Domain\User\Data\UserBadge;
use App\Service\LuminosityContrast;
use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeZone;
use JsonSerializable;

class Event
{
    public function __construct(
        private int $id,
        private DateTimeImmutable $start,
        private DateTimeImmutable $end,
        private DateTimeImmutable $created,
        private UserBadge $creator,
        private ?DateTimeImmutable $edited,
        private string $name,
        private string $description,
        private EventTypeEnum $type,
        private array $allowedRoles = [],
        private array $allowedRoleIds = []
    ) {
    }

    public static function new(object $data)
    {
        return new self(
            id: $data->id,
            start: new DateTimeImmutable($data->start, new DateTimeZone('UTC')),
            end: new DateTimeImmutable($data->end, new DateTimeZone('UTC')),
            created: new DateTimeImmutable($data->created, new DateTimeZone('UTC')),
            creator: new UserBadge($data->creator, $data->email),
            edited: $data->edited ? new DateTimeImmutable($data->edited, new DateTimeZone('UTC')) : null,
            name: $data->name,
            description: $data->description,
            type: EventTypeEnum::tryFrom($data->type),
        );
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getStart(): DateTimeImmutable
    {
        return $this->start;
    }

    public function getEnd(): DateTimeImmutable
    {
        return $this->end;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDesc(): string
    {
        return $this->description;
    }

    public function getType(): EventTypeEnum
    {
        return $this->type;
    }

    public function getCreator(): UserBadge
    {
        return $this->creator;
    }

    public function getAllowedRoles(): array
    {
        return $this->allowedRoles;
    }

    public function setAllowedRoles(array $allowedRoles): self
    {
        foreach ($allowedRoles as $a) {
            if (ProfileType::CUSTOM != $a->getType()) {
                $this->allowedRoleIds[] = $a->getType()->value;
            } else {
                $this->allowedRoleIds[] = $a->getId();
            }
        }
        $this->allowedRoles = $allowedRoles;

        return $this;
    }

    public function getAllowedRoleIds(): array
    {
        return $this->allowedRoleIds;
    }

    public function getEdited(): ?DateTimeImmutable
    {
        return $this->edited;
    }

    public function getSimpleCalendarEntry(): array
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getName(),
            'backgroundColor' => $this->getType()->getForeColor(),
            'textColor' => LuminosityContrast::getContrastColor($this->getType()->getForeColor()),
            'start' => $this->getStart()->format(DateTimeImmutable::ATOM),
            'end' => $this->getEnd()->format(DateTimeImmutable::ATOM),
            'allDay' => false,
            'url' => '/event/' . $this->getId()
        ];
    }
}
