<?php

namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class InvalidLoginCredentials extends HttpException
{

    public function __construct(
        string $message,
        int $code = 401,
        Throwable $previous = null
    ) {
        parent::__construct($code, $message, $previous);
    }
}
