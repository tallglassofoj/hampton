<?php

declare(strict_types=1);

namespace App\Extension\Twig;

use Slim\Csrf\Guard;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;
use Twig\TwigFunction;

class SlimCsrfExtension extends AbstractExtension
{
    public function __construct(
        private Guard $csrf
    ) {

    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'csrf',
                [
                    $this, 'renderCsrfTags'
                ],
                [
                    'is_safe' => ['html']
                ]
            )
        ];
    }

    public function renderCsrfTags(): string
    {
        $name = sprintf(
            "<input type='hidden' name='%s' value='%s' />",
            $this->csrf->getTokenNameKey(),
            $this->csrf->getTokenName()
        );
        $value = sprintf(
            "<input type='hidden' name='%s' value='%s' />",
            $this->csrf->getTokenValueKey(),
            $this->csrf->getTokenValue()
        );
        return $name."\n".$value;
    }
}
