<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\DBAL\Query\QueryBuilder;

interface BaseQuery
{
    public function getBaseQuery(): QueryBuilder;
}
