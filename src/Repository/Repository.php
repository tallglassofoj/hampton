<?php

namespace App\Repository;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Result;

class Repository
{
    public ?string $entityClass = null;

    public function __construct(
        public Connection $connection
    ) {
    }

    public function qb(): QueryBuilder
    {
        return $this->connection->createQueryBuilder();
    }

    public function getResults(
        Result $result,
        ?string $class = null
    ): array {
        $results = $result->fetchAllAssociative();
        if (!$class) {
            $class = $this->entityClass;
        }
        foreach ($results as &$r) {
            $r = $class::new((object) $r);
        }
        return $results;
    }

    public function getResult(Result $result, ?string $class = null): mixed
    {
        $data = $result->fetchAssociative();
        if(!$data) {
            return null;
        }
        if(!$class) {
            $class = $this->entityClass;
        }
        return $class::new((object) $data);
    }
}
