<?php

declare(strict_types=1);

use App\Domain\User\Service\ValidateUserService;
use App\Exception\ValidationException;
use PHPUnit\Framework\TestCase;

final class UserValidationTest extends TestCase
{

    private $testCase = [
        'firstName' => 'John',
        'lastName' => 'Doe',
        'email' => 'email@mail.com',
        'password' => 'cie6ugh0Doo8vahn5nah3thooX7ee2lo'
    ];

    public function testValidRequest(): void
    {
        $test = ValidateUserService::validateUser($this->testCase);
        $this->assertNull($test);
    }

    public function testMissingFirstName(): void
    {
        $this->expectException(ValidationException::class);
        $case = $this->testCase;
        unset($case['firstName']);
        $test = ValidateUserService::validateUser($case);
        $this->assertNull($test);
    }
    public function testEmptyFirstName(): void
    {
        $this->expectException(ValidationException::class);
        $case = $this->testCase;
        $case['firstName'] = '';
        $test = ValidateUserService::validateUser($case);
        $this->assertNull($test);
    }

    public function testMissingLastName(): void
    {
        $this->expectException(ValidationException::class);
        $case = $this->testCase;
        unset($case['lastName']);
        $test = ValidateUserService::validateUser($case);
        $this->assertNull($test);
    }

    public function testEmptyLastName(): void
    {
        $this->expectException(ValidationException::class);
        $case = $this->testCase;
        $case['lastName'] = '';
        $test = ValidateUserService::validateUser($case);
        $this->assertNull($test);
    }

    public function testMissingEmail(): void
    {
        $this->expectException(ValidationException::class);
        $case = $this->testCase;
        unset($case['email']);
        $test = ValidateUserService::validateUser($case);
        $this->assertNull($test);
    }

    public function testEmptyEmail(): void
    {
        $this->expectException(ValidationException::class);
        $case = $this->testCase;
        $case['email'] = '';
        $test = ValidateUserService::validateUser($case);
        $this->assertNull($test);
    }

    public function testInvalidEmail(): void
    {
        $this->expectException(ValidationException::class);
        $case = $this->testCase;
        $case['email'] = 'test at email.com';
        $test = ValidateUserService::validateUser($case);
        $this->assertNull($test);
    }

    public function testMissingPassword(): void
    {
        $this->expectException(ValidationException::class);
        $case = $this->testCase;
        unset($case['password']);
        $test = ValidateUserService::validateUser($case);
        $this->assertNull($test);
    }

    public function testEmptyPassword(): void
    {
        $this->expectException(ValidationException::class);
        $case = $this->testCase;
        $case['password'] = '';
        $test = ValidateUserService::validateUser($case);
        $this->assertNull($test);
    }
}
