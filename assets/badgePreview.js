function hexToRgb(hex) {
  const shortHexRegExp = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  hex = hex.replace(shortHexRegExp, (_, r, g, b) => r + r + g + g + b + b);
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

  if (!result) throw Error("A valid HEX must be provided");

  return {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16),
  };
}

function setContrastColor(color) {
  const { r, g, b } = hexToRgb(color);
  const yiq = (r * 299 + g * 587 + b * 114) / 1000;

  let contrastColor;

  if (yiq >= 128) {
    contrastColor = "#000";
  } else {
    contrastColor = "#fff";
  }

  return contrastColor;
}

function newIcon(v) {
  let newIcon = document.createElement("i");
  let classes = v.replace(/(<i class=")([a-z\-\W]+)("><\/i>)/i, "$2");
  icon.value = classes;
  classes.split(" ").forEach((c) => {
    newIcon.classList.add(c);
  });
  return newIcon;
}

const name = document.querySelector("#name");
const color = document.querySelector("#color");
const icon = document.querySelector("#icon");

const previewName = document.querySelector("#preview-name");
const previewBadge = document.querySelector("#preview");
const previewIcon = document.querySelector("#preview-icon");

document.querySelector("#newRoleForm").addEventListener("input", (e) => {
  previewName.textContent = name.value;
  previewBadge.style.backgroundColor = color.value;
  previewBadge.style.color = setContrastColor(color.value);
  previewIcon.replaceWith(newIcon(icon.value));
});
