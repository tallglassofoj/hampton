// assets/app.js
/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import "./styles/app.scss";
import * as bootstrap from "bootstrap";

const tooltipTriggerList = document.querySelectorAll(
  '[data-bs-toggle="tooltip"]'
);
const tooltipList = [...tooltipTriggerList].map(
  (tooltipTriggerEl) => new bootstrap.Tooltip(tooltipTriggerEl)
);

const popoverTriggerList = document.querySelectorAll(
  '[data-bs-toggle="popover"]'
);
const popoverList = [...popoverTriggerList].map(
  (popoverTriggerEl) => new bootstrap.Popover(popoverTriggerEl)
);

import { format } from "date-fns";
import { zonedTimeToUtc } from "date-fns-tz";

const timeElements = document.querySelectorAll("time");
timeElements.forEach((e) => {
  const time = new Date(e.textContent);
  e.setAttribute("title", e.textContent);
  e.setAttribute("data-bs-toggle", "tooltip");
  e.textContent = format(zonedTimeToUtc(time), "PPP 'at' HH:mm");
});

// const timeInputs = document.querySelectorAll("input[type=datetime-local]");
// timeInputs.forEach((e) => {
//   const time = new Date(e.value);
//   e.value = format(zonedTimeToUtc(time), "yyyy-MM-dd'T'HH:mm");
// });

document.querySelectorAll("[data-href]").forEach((r) => {
  r.addEventListener("click", (e) => {
    window.location = r.dataset.href;
  });
});

document.querySelectorAll("[name=timezone]").forEach((t) => {
  t.value = Intl.DateTimeFormat().resolvedOptions().timeZone;
});
