(() => {
  "use strict";

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  const forms = document.querySelectorAll(".needs-validation");

  // Loop over them and prevent submission
  Array.from(forms).forEach((form) => {
    form.addEventListener(
      "submit",
      (event) => {
        if (!form.checkValidity()) {
          event.preventDefault();
          event.stopPropagation();
        }

        form.classList.add("was-validated");
      },
      false
    );
  });
})();

document.querySelectorAll(".event-type").forEach((r) => {
  r.addEventListener("change", (e) => {
    const inmate = document.querySelector("#role-Inmate");
    const guard = document.querySelector("#role-Guard");
    if ("Roleplay" == r.value) {
      if (!guard.checked) {
        guard.click();
      }
      if (!inmate.checked) {
        inmate.click();
      }
      guard.setAttribute("disabled", "");
      inmate.setAttribute("disabled", "");
    } else {
      guard.removeAttribute("disabled");
      inmate.removeAttribute("disabled");
    }
  });
});

document.querySelector("[name=timezone]").value =
  Intl.DateTimeFormat().resolvedOptions().timeZone;
